package com.turn.marshall.client;

import com.turn.marshall.shared.avro.generated.PropertyMessage;

/**
 * @author Chunzhao Zheng
 */
public interface PropertiesReceiver
{
    public void receive(PropertyMessage message);
}
