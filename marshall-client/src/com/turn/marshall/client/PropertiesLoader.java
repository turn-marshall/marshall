/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.client;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.turn.marshall.shared.PropertyScope;
import com.turn.marshall.shared.PropertyKey;
import com.turn.marshall.shared.ServiceEndpoint;
import com.turn.marshall.shared.avro.ByteBufferAdapter;
import com.turn.marshall.shared.avro.generated.PropertyMessage;

/**
 * @author Chunzhao Zheng
 */
public class PropertiesLoader
{
    public PropertiesLoader()
    {
        client_ = new AsyncHttpClient();
        adapter_ = new ByteBufferAdapter<PropertyMessage>(PropertyMessage.class);
        watcher_ = new Stopwatch();
    }

    /**
     * Loads the property value from remote server for the given {@link com.turn.marshall.shared.PropertyKey}
     * within the scope that is specified by the given {@link com.turn.marshall.shared.PropertyScope}.
     */
    PropertyMessage load(final PropertyScope scope, final PropertyKey key) throws IOException
    {
        String url = url(ServiceEndpoint.PROPERTY_GET);

        AsyncHttpClient.BoundRequestBuilder request = client_.preparePost(url)
                                                             .setBody(scope.toJson())
                                                             .addQueryParameter("name", key.name())
                                                             .addQueryParameter("group", key.group());

        Future<PropertyMessage> future = request.execute(new AsyncCompletionHandler<PropertyMessage>()
        {
            @Override
            public PropertyMessage onCompleted(Response response) throws Exception
            {
                if (!valid(response)) {
                    String error = "Failed to retrieve property message response from " + response.getUri();
                    throw new IOException(error);
                }

                LOG.info("Completed load message receiving from property server");

                ByteBuffer buffer = response.getResponseBodyAsByteBuffer();
                return adapter_.fromByteBuffer(buffer);
            }

            @Override
            public void onThrowable(Throwable cause)
            {
                LOG.warn("Experienced problem to receive load message from property server: " + cause.getMessage());
            }
        });

        watcher_.reset();
        watcher_.start();

        PropertyMessage message;
        try {
            message = future.get();
        } catch (InterruptedException e) {
            String error = "The operation of getting property message was interrupted: " + e.getMessage();
            throw new IOException(error, e);
        } catch (ExecutionException e) {
            String error = "The operation of getting property message was failed on execution: " + e.getMessage();
            throw new IOException(error, e);
        } finally {
            watcher_.stop();
        }

        long mills = watcher_.elapsed(TimeUnit.MILLISECONDS);
        LOG.info("load() spent " + mills + " milliseoncs to complete receiving the property message");

        return message;
    }

    /**
     * Loads all {@link PropertyKey} and its assoicated value from remote server
     * within the scope that is specified by the given {@link com.turn.marshall.shared.PropertyScope}.
     */
    void loadAll(final PropertyScope scope, final PropertiesReceiver receiver) throws IOException
    {
        String url = url(ServiceEndpoint.PROPERTY_GET_ALL);

        AsyncHttpClient.BoundRequestBuilder request = client_.preparePost(url).setBody(scope.toJson());

        Future<PropertyMessage> future = request.execute(new AsyncCompletionHandler<PropertyMessage>()
        {
            @Override
            public PropertyMessage onCompleted(Response response) throws IOException
            {
                if (!valid(response)) {
                    String error = "Failed to retrieve property message response from " + response.getUri();
                    throw new IOException(error);
                }

                LOG.info("Completed loadAll message receiving from property server");

                ByteBuffer buffer = response.getResponseBodyAsByteBuffer();
                PropertyMessage message = adapter_.fromByteBuffer(buffer);
                if (message != null) {
                    receiver.receive(message);
                }
                return message;
            }

            @Override
            public void onThrowable(Throwable cause)
            {
                LOG.warn("Experienced problem to receive loadAll message from property server: " + cause.getMessage());
            }
        });
    }

    String url(ServiceEndpoint endpoint)
    {
        return PropertiesOption.SERVER_URL.value() + PropertiesOption.SERVER_CONTEXT.value() + endpoint.path();
    }

    boolean valid(Response response)
    {
        return response.getStatusCode() == 200 && response.hasResponseBody();
    }

    private final AsyncHttpClient client_;
    private final ByteBufferAdapter<PropertyMessage> adapter_;
    private final Stopwatch watcher_;

    private final static Log LOG = LogFactory.getLog(PropertiesLoader.class);
}
