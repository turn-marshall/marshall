/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.client;

import java.util.Set;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

import com.turn.marshall.shared.PropertyScope;
import com.turn.marshall.shared.ScopeType;

/**
 * @author Chunzhao Zheng
 */
public class PropertiesSetting implements Reusable
{
    final static ObjectFactory<PropertiesSetting> FACTORY = new ObjectFactory<PropertiesSetting>()
    {
        @Override
        protected PropertiesSetting create()
        {
            return new PropertiesSetting();
        }
    };

    PropertiesSetting()
    {
        scope_ = PropertyScope.create();
    }

    public static PropertiesSetting create()
    {
        return FACTORY.object();
    }

    @Override
    public void reset()
    {
        scope_.reset();
    }

    void override(PropertiesSetting setting)
    {
        PropertyScope scope = setting.scope();
        Set<ScopeType> types = scope.getScopeTypes();
        for (ScopeType type : types) {
            String value = scope.getScopeValue(type);
            scope_.addScope(type, value); // merge the given scope into this scope
        }
    }

    public PropertyScope scope()
    {
        return scope_;
    }

    public PropertiesSetting scope(ScopeType type, String value)
    {
        scope_.addScope(type, value);
        return this;
    }

    private final PropertyScope scope_;
}
