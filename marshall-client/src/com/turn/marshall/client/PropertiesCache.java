package com.turn.marshall.client;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.AbstractScheduledService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.turn.marshall.shared.PropertyScope;
import com.turn.marshall.shared.PropertyKey;
import com.turn.marshall.shared.avro.generated.PropertyMessage;
import com.turn.marshall.shared.avro.generated.PropertyRecord;

/**
 * @author Chunzhao Zheng Zheng
 */
public class PropertiesCache
{
    final CacheLoader<PropertyKey, String> CACHE_LOADER = new CacheLoader<PropertyKey, String>()
    {
        @Override
        public String load(PropertyKey key) throws IOException
        {
            PropertyScope scope = setting_.scope();
            PropertyMessage message = loader_.load(scope, key);
            List<PropertyRecord> records = message.getRecords();
            if (records.isEmpty()) {
                return ""; //TODO: use PropertyValue.EMPTY instead
            }
            PropertyRecord record = records.get(0);
            return record.getValue().toString();
        }
    };

    final AbstractScheduledService CACHE_SCHEDULER = new AbstractScheduledService()
    {
        @Override
        protected void runOneIteration() throws IOException
        {
            refresh(); // reload all properties into properties cache
        }

        @Override
        protected Scheduler scheduler()
        {
            int interval = PropertiesOption.CACHE_RELOAD_INTERVAL.integer();
            return Scheduler.newFixedRateSchedule(0, interval, TimeUnit.SECONDS);
        }
    };

    public PropertiesCache()
    {
        cache_ = CacheBuilder.newBuilder().build(CACHE_LOADER);
        loader_ = new PropertiesLoader();
        setting_ = new PropertiesSetting();
    }

    void configure(PropertiesSetting setting)
    {
        setting_.override(setting);
    }

    void initialize()
    {
        // load all properties into properties cache
        refresh();

        // start cache schedulers if enabled
        boolean enabled = PropertiesOption.CACHE_RELOAD_ENABLED.bool();
        if (enabled) {
            CACHE_SCHEDULER.start();
        }
    }

    void dispose()
    {
        // dispose cached properties
        cache_.cleanUp();
        cache_.asMap().clear();

        // shutdown cache scheduler if enabled
        boolean enabled = PropertiesOption.CACHE_RELOAD_ENABLED.bool();
        if (enabled) {
            CACHE_SCHEDULER.stop();
        }
    }

    void refresh()
    {
        PropertyScope scope = setting_.scope();

        // load all properties via properties loader
        try {
            loader_.loadAll(scope, new PropertiesReceiver()
            {
                @Override
                public void receive(PropertyMessage message)
                {
                    // add loaded properties into properties cache
                    add(message);
                }
            });
        } catch (IOException e) {
            LOG.warn("Failed to load all properties from configuration server: " + e.getMessage());
        }
    }

    void add(PropertyMessage message)
    {
        List<PropertyRecord> records = message.getRecords();
        if (records == null) {
            LOG.warn("The property record list included in the property message is NULL");
            return;
        }
        LOG.info("Loaded [" + records.size() + "] property records from configuration server");

        for (PropertyRecord record : records) {
            PropertyKey key = PropertyKey.create(record.getName().toString(), record.getGroup().toString());
            cache_.asMap().put(key, record.getValue().toString());
        }
    }

    @Nullable
    public String get(PropertyKey key)
    {
        return cache_.getIfPresent(key);
    }

    public Set<PropertyKey> keys()
    {
        return cache_.asMap().keySet();
    }

    public long size()
    {
        return cache_.size();
    }

    public void set(PropertyKey key, String value)
    {
        cache_.put(key, value);
    }

    private final LoadingCache<PropertyKey, String> cache_;
    private final PropertiesLoader loader_;
    private final PropertiesSetting setting_;

    private final static Log LOG = LogFactory.getLog(PropertiesCache.class);
}
