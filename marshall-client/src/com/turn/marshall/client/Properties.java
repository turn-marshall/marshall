package com.turn.marshall.client;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

import com.google.common.collect.Maps;

import com.turn.marshall.shared.PropertyKey;

/**
 * @author Chunzhao Zheng Zheng
 */
public class Properties
{
    Properties()
    {
        cache_ = new PropertiesCache();
    }

    public static Properties getInstance()
    {
        return INSTANCE;
    }

    public void configure(PropertiesSetting setting)
    {
        cache_.configure(setting);
    }

    public void initialize() throws IOException
    {
        // initialize properties config
        PropertiesConfig.getInstance().initialize();

        // initialize properties cache
        cache_.initialize();
    }

    public void dispose()
    {
        // dispose properties cache
        cache_.dispose();
    }

    PropertyKey internalize(String name, String group)
    {
        return PropertyKey.create(name, group);
    }

    @Nullable
    public String getProperty(String name)
    {
        return getProperty(name, null);
    }

    @Nullable
    public String getProperty(String name, @Nullable String group)
    {
        PropertyKey internal = internalize(name, group);
        return cache_.get(internal);
    }

    public Map<String, String> getProperties()
    {
        Map<String, String> properties = Maps.newHashMap();
        Set<PropertyKey> keys = cache_.keys();
        for (PropertyKey key : keys) {
            String value = cache_.get(key);
            properties.put(key.name(), value);
        }
        return properties;
    }

    public void setProperty(String name, String value)
    {
        setProperty(name, null, value);
    }

    public void setProperty(String name, @Nullable String group, String value)
    {
        PropertyKey internal = internalize(name, group);
        cache_.set(internal, value);
    }

    private final PropertiesCache cache_;

    private final static Properties INSTANCE = new Properties();
}
