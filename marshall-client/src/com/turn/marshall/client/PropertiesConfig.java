package com.turn.marshall.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Chunzhao Zheng
 */
class PropertiesConfig
{
    PropertiesConfig()
    {
        properties_ = new Properties();
    }

    void initialize() throws IOException
    {
        InputStream resource = resource();
        properties_.load(resource);
    }

    InputStream resource()
    {
        return Thread.currentThread()
                     .getContextClassLoader()
                     .getResourceAsStream("marshall.properties");

    }

    static PropertiesConfig getInstance()
    {
        return INSTANCE;
    }

    String getProperty(String key)
    {
        return properties_.getProperty(key);
    }

    private final Properties properties_;
    private final static PropertiesConfig INSTANCE = new PropertiesConfig();
}
