/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.client;

import com.google.inject.AbstractModule;

/**
 * @author Chunzhao Zheng
 */
public class PropertiesModule extends AbstractModule
{
    @Override
    protected void configure()
    {
    }
}
