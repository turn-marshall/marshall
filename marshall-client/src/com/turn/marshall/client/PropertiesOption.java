/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.client;

import org.apache.commons.lang.StringUtils;

/**
 * @author Chunzhao Zheng
 */
public enum PropertiesOption
{
    SERVER_URL("marshall.server.url", "http://localhost:8080"),
    SERVER_CONTEXT("marshall.server.context", "/marshall/api/"),
    CACHE_RELOAD_ENABLED("marshall.cache.reload.enabled", true),
    CACHE_RELOAD_INTERVAL("marshall.cache.reload.interval", 1),
    ;

    PropertiesOption(String key, Object value)
    {
        key_ = key;
        value_ = value;
    }

    public String key()
    {
        return key_;
    }

    public String value()
    {
        return PropertiesConfig.getInstance().getProperty(key());
    }

    public int integer()
    {
        String value = value();
        if (StringUtils.isEmpty(value)) {
            return Integer.class.cast(value_);
        }
        return Integer.valueOf(value);
    }

    public boolean bool()
    {
        String value = value();
        if (StringUtils.isEmpty(value)) {
            return Boolean.class.cast(value_);
        }
        return Boolean.valueOf(value);
    }

    private final String key_;
    private final Object value_; // default value
}
