/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared;

/**
 * @author Chunzhao Zheng
 */
public enum AppStage
{
    DEVELOPMENT,
    SANDBOX,
    PRODUCTION,
    ;

    AppStage()
    {
        alias_ = name().toLowerCase();
    }

    public String alias()
    {
        return alias_;
    }

    public static AppStage from(String alias)
    {
        for (AppStage stage : AppStage.values()) {
            if (stage.alias().equals(alias)) {
                return stage;
            }
        }
        return DEVELOPMENT;
    }

    private final String alias_;
}
