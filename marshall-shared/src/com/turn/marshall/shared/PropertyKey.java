/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared;

import com.google.common.base.Objects;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

/**
 * @author Chunzhao Zheng
 */
public class PropertyKey implements Reusable
{
    final static ObjectFactory<PropertyKey> FACTORY = new ObjectFactory<PropertyKey>()
    {
        @Override
        protected PropertyKey create()
        {
            return new PropertyKey();
        }
    };

    PropertyKey() {}

    @Override
    public void reset()
    {
        group_ = null;
        name_ = null;
    }

    public static PropertyKey create(String name, String group)
    {
        return FACTORY.object().name(name).group(group);
    }

    public String name()
    {
        return name_;
    }

    public PropertyKey name(String name)
    {
        name_ = name;
        return this;
    }

    public String group()
    {
        return group_;
    }

    public PropertyKey group(String group)
    {
        group_ = group;
        return this;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) {
            return false;
        }

        if (!PropertyKey.class.equals(obj.getClass())) {
            return false;
        }

        PropertyKey another = PropertyKey.class.cast(obj);

        return Objects.equal(group(), another.group()) &&
               Objects.equal(name(), another.name());
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(group(), name());
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(PropertyKey.class)
                      .omitNullValues()
                      .add("group", group())
                      .add("name", name())
                      .toString();
    }

    private String name_;
    private String group_;
}
