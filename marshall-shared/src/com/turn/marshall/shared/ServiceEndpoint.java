/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared;

/**
 * @author Chunzhao Zheng
 */
public enum ServiceEndpoint
{
    PROPERTY_GET("property"),
    PROPERTY_GET_ALL("properties"),
    PROPERTY_CREATE("property/create"),
    PROPERTY_UPDATE("property/update"),
    PROPERTY_DELETE("property/delete"),

    PROPERTY_GROUP_GET("group"),
    PROPERTY_GROUP_GET_ALL("groups"),
    PROPERTY_GROUP_CREATE("group/create"),
    PROPERTY_GROUP_UPDATE("group/update"),
    PROPERTY_GROUP_DELETE("group/delete"),

    PROPERTY_SCOPE_GET("scope"),
    PROPERTY_SCOPE_GET_ALL("scopes"),
    PROPERTY_SCOPE_CREATE("scope/create"),
    PROPERTY_SCOPE_UPDATE("scope/update"),
    PROPERTY_SCOPE_DELETE("scope/delete"),
    
    CONTROLLER_PROPERTY("controller/property"),
    CONTROLLER_PROPERTY_GET_ALL("controller/properties"),
//    CONTROLLER_PROPERTY_CREATE("controller/property/create"),
//    CONTROLLER_PROPERTY_DELETE("controllerproperty/delete"),
    
    CONTROLLER_AUDIT("controller/audit"),
    CONTROLLER_AUDITS("controller/audits"),
    CONTROLLER_GROUP_GET_ALL("controller/groups"),
    CONTROLLER_SCOPE_GET_ALL("controller/scopes"),
    ;

    ServiceEndpoint(String path)
    {
        path_ = path;
    }

    public String path()
    {
        return path_;
    }

    private final String path_;
}
