package com.turn.marshall.shared;

/**
 * @author Chunzhao Zheng
 */
public enum EntityStatus
{
    UNKNOWN(-1),
    ACTIVED(1),
    DEACTIVED(2),
    DELETED(5),
    ;

    EntityStatus(int code)
    {
        code_ = code;
    }

    public int code()
    {
        return code_;
    }

    public static EntityStatus from(int code)
    {
        for (EntityStatus status : EntityStatus.values()) {
            if (status.code() == code) {
                return status;
            }
        }
        return UNKNOWN;
    }

    private final int code_;
}
