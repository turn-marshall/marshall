package com.turn.marshall.shared;

/**
 * @author Chunzhao Zheng
 */
public enum UserAction
{
    UNKNOWN(-1),
    CREATE(1),
    MODIFY(2),
    DELETE(3),
    ACTIVATE(4),
    DEACTIVATE(5),
    ;

    UserAction(int code)
    {
        code_ = code;
    }

    public int code()
    {
        return code_;
    }

    public static UserAction from(int code)
    {
        for (UserAction action : UserAction.values()) {
            if (action.code() == code) {
                return action;
            }
        }
        return UNKNOWN;
    }

    private final int code_;
}
