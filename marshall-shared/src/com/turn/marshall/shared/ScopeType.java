/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared;

import java.util.Arrays;
import java.util.Comparator;

import javax.annotation.Nullable;

/**
 * @author Chunzhao Zheng
 */
public enum ScopeType
{
    DEFAULT(-1, null, 0),
    DATA_CENTER(1, "ds", 1),
    HOST_NAME(2, "host", 4),
    HOST_IP(3, "ip", 5),
    SERVER_ID(4, "sid", 6),
    SERVER_TYPE(5, "stype", 2),
    SERVER_GROUP(6, "sgroup", 3),
    ;

    ScopeType(int code, @Nullable String alias, int priority)
    {
        code_ = code;
        alias_ = alias;
        priority_ = priority;
    }

    public int code()
    {
        return code_;
    }

    @Nullable
    public String alias()
    {
        return alias_;
    }
    
    public int priority()
    {
        return priority_;
    }

    @Nullable
    public static ScopeType from(int code)
    {
        for (ScopeType type : ScopeType.values()) {
            if (type.code() == code) {
                return type;
            }
        }
        return DEFAULT;
    }
    
    public static ScopeType[] valuesInAscendingPriorityOrder() {
    	Comparator<ScopeType> priorityComparator = new Comparator<ScopeType>() {
  		  public int compare(ScopeType e1, ScopeType e2) {
  		     if (e1.priority() >= e2.priority())
  		       return 1;
  		     return 0;
  		  }
  		};
  		ScopeType[] result = values();
    	Arrays.sort(result, priorityComparator);
    	return result;
    }

    private final int code_;
    private final String alias_;
    private final int priority_;
}
