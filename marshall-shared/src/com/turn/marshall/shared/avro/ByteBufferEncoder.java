package com.turn.marshall.shared.avro;

import java.io.EOFException;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.LinkedList;

import javax.annotation.concurrent.NotThreadSafe;

import com.google.common.collect.Lists;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

import org.apache.avro.io.BinaryEncoder;

/**
 * @author Chunzhao Zheng
 */
@NotThreadSafe
public class ByteBufferEncoder extends BinaryEncoder implements Reusable
{
    final static ObjectFactory<ByteBufferEncoder> FACTORY = new ObjectFactory<ByteBufferEncoder>()
    {
        @Override
        protected ByteBufferEncoder create()
        {
            return new ByteBufferEncoder();
        }
    };

    protected ByteBufferEncoder()
    {
        buffers_ = Lists.newLinkedList();
        addByteBuffer();
    }

    public static ByteBufferEncoder get()
    {
        return FACTORY.object();
    }

    @Override
    public void reset()
    {
        buffers_.clear();
        addByteBuffer();
    }

    @Override
    public int bytesBuffered()
    {
        return 0;
    }

    ByteBuffer addByteBuffer()
    {
        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
        buffers_.addLast(buffer);
        return buffer;
    }

    public ByteBuffer getByteBuffer()
    {
        return ByteBuffers.merge(buffers_);
    }

    @Override
    public void flush() throws IOException
    {
        for (ByteBuffer buffer : buffers_) {
            buffer.flip();
        }
    }

    protected void write(byte b) throws IOException
    {
        ByteBuffer buffer = buffers_.getLast();
        if (!buffer.hasRemaining()) {
            buffer = addByteBuffer();
        }
        try {
            buffer.put(b);
        } catch (BufferOverflowException e) {
            throw new EOFException();
        }
    }

    protected void write(byte[] bytes, int off, int length) throws IOException
    {
        ByteBuffer buffer = buffers_.getLast();
        int remaining = buffer.remaining();
        while (length > remaining) {
            buffer.put(bytes, off, remaining);
            length -= remaining;
            off += remaining;
            buffer = addByteBuffer();
            remaining = buffer.remaining();
        }
        buffer.put(bytes, off, length);
    }

    protected void write(int val) throws IOException
    {
        byte b = (byte) (val & 0xFF);
        write(b);
    }

    protected void write(long val) throws IOException
    {
        byte b = (byte) (val & 0xFF);
        write(b);
    }

    @Override
    public void writeBoolean(boolean b) throws IOException
    {
        write(b ? (byte) 1 : (byte) 0);
    }

    @Override
    public void writeInt(int n) throws IOException
    {
        // move sign to low-order bit, and flip others if negative
        n = (n << 1) ^ (n >> 31);
        if ((n & ~0x7F) != 0) {
            write(n | 0x80);
            n >>>= 7;
            if (n > 0x7F) {
                write(n | 0x80);
                n >>>= 7;
                if (n > 0x7F) {
                    write(n | 0x80);
                    n >>>= 7;
                    if (n > 0x7F) {
                        write(n | 0x80);
                        n >>>= 7;
                    }
                }
            }
        }
        write((byte) n);
    }

    @Override
    public void writeLong(long n) throws IOException
    {
        // move sign to low-order bit, and flip others if negative
        n = (n << 1) ^ (n >> 63);
        if ((n & ~0x7FL) != 0) {
            write((int) (n | 0x80));
            n >>>= 7;
            if (n > 0x7F) {
                write(n | 0x80);
                n >>>= 7;
                if (n > 0x7F) {
                    write(n | 0x80);
                    n >>>= 7;
                    if (n > 0x7F) {
                        write(n | 0x80);
                        n >>>= 7;
                        if (n > 0x7F) {
                            write(n | 0x80);
                            n >>>= 7;
                            if (n > 0x7F) {
                                write(n | 0x80);
                                n >>>= 7;
                                if (n > 0x7F) {
                                    write(n | 0x80);
                                    n >>>= 7;
                                    if (n > 0x7F) {
                                        write(n | 0x80);
                                        n >>>= 7;
                                        if (n > 0x7F) {
                                            write(n | 0x80);
                                            n >>>= 7;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        write((byte) n);
    }

    @Override
    public void writeFloat(float f) throws IOException
    {
        int bits = Float.floatToRawIntBits(f);
        // hotspot compiler works well with this variant
        write(bits);
        write(bits >>> 8);
        write(bits >>> 16);
        write(bits >>> 24);
    }

    @Override
    public void writeDouble(double d) throws IOException
    {
        long bits = Double.doubleToRawLongBits(d);
        int first = (int) bits;
        int second = (int) (bits >>> 32);

        write(first);
        write(first >>> 8);
        write(first >>> 16);
        write(first >>> 24);
        write(second);
        write(second >>> 8);
        write(second >>> 16);
        write(second >>> 24);
    }

    @Override
    public void writeFixed(byte[] bytes, int start, int length) throws IOException
    {
        write(bytes, start, length);
    }

    @Override
    protected void writeZero() throws IOException
    {
        write(0);
    }

    private final LinkedList<ByteBuffer> buffers_;
    private final static int BUFFER_SIZE = 8 * 1024;
}
