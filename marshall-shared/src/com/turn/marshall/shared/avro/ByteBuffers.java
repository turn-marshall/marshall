/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared.avro;

import java.nio.ByteBuffer;

/**
 * @author Chunzhao Zheng
 */
public final class ByteBuffers
{
    public static ByteBuffer merge(Iterable<ByteBuffer> buffers)
    {
        int size = 0;
        for (ByteBuffer buffer : buffers) {
            size += buffer.remaining();
        }

        ByteBuffer merged = ByteBuffer.allocate(size);
        for (ByteBuffer buffer : buffers) {
            merged.put(buffer);
        }
        merged.flip();
        return merged;
    }

    public static ByteBuffer fromArray(byte[] bytes)
    {
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        buffer.put(bytes);
        return buffer;
    }

    public static byte[] toArray(ByteBuffer buffer)
    {
        byte[] bytes;

        if (buffer.hasArray()) {
            bytes = buffer.array();
        } else {
            bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
        }

        return bytes;
    }
}
