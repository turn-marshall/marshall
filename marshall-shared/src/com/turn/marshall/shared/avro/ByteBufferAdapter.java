/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared.avro;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.avro.Schema;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecord;

/**
 * <S> A concrete {@link SpecificRecord} type.
 *
 * @author Chunzhao Zheng
 */
public class ByteBufferAdapter<S extends SpecificRecord>
{
    public ByteBufferAdapter(Class<S> type)
    {
        reader_ = new SpecificDatumReader<S>(type);
        writer_ = new SpecificDatumWriter<S>(type);

        decoder_ = ByteBufferDecoder.get();
        encoder_ = ByteBufferEncoder.get();
    }

    public ByteBufferAdapter(Schema schema)
    {
        reader_ = new SpecificDatumReader<S>(schema);
        writer_ = new SpecificDatumWriter<S>(schema);

        decoder_ = ByteBufferDecoder.get();
        encoder_ = ByteBufferEncoder.get();
    }

    public S fromByteBuffer(ByteBuffer buffer)
    {
        decoder_.setByteBuffer(buffer);
        try {
            return reader_.read(null, decoder_);
        } catch (IOException e) {
            String error = "Failed to decode byte buffer to avro record: " + e.getMessage();
            throw new RuntimeException(error, e);
        } finally {
            decoder_.reset();
        }
    }

    public ByteBuffer toByteBuffer(S record)
    {
        try {
            writer_.write(record, encoder_);
            encoder_.flush();
            return encoder_.getByteBuffer();
        } catch (IOException e) {
            String error = "Failed to encode avro record to byte buffer: " + e.getMessage();
            throw new RuntimeException(error, e);
        } finally {
            encoder_.reset();
        }
    }

    private final DatumReader<S> reader_;
    private final DatumWriter<S> writer_;

    private final ByteBufferDecoder decoder_;
    private final ByteBufferEncoder encoder_;
}
