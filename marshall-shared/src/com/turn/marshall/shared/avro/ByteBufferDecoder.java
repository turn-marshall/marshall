/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared.avro;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.List;

import javax.annotation.concurrent.NotThreadSafe;

import com.google.common.collect.Lists;
import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.util.ByteBufferInputStream;

/**
 * @author Chunzhao Zheng
 */
@NotThreadSafe
public class ByteBufferDecoder extends BinaryDecoder implements Reusable
{
    final static ObjectFactory<ByteBufferDecoder> FACTORY = new ObjectFactory<ByteBufferDecoder>()
    {
        @Override
        protected ByteBufferDecoder create()
        {
            return new ByteBufferDecoder();
        }
    };

    protected ByteBufferDecoder()
    {
        buffer_ = EMPTY_BUFFER;
    }

    public static ByteBufferDecoder get()
    {
        return FACTORY.object();
    }

    public void setByteBuffer(ByteBuffer buffer)
    {
        buffer_ = buffer;
    }

    @Override
    public void reset()
    {
        buffer_ = EMPTY_BUFFER;
    }

    protected int read() throws IOException
    {
        try {
            return buffer_.get() & 0xff;
        } catch (BufferUnderflowException e) {
            throw new EOFException();
        }
    }

    @Override
    public boolean readBoolean() throws IOException
    {
        int n = read();
        return n == 1;
    }

    @Override
    public int readInt() throws IOException
    {
        int b = read();
        int n = b & 0x7f;
        if (b > 0x7f) {
            b = read();
            n ^= (b & 0x7f) << 7;
            if (b > 0x7f) {
                b = read();
                n ^= (b & 0x7f) << 14;
                if (b > 0x7f) {
                    b = read();
                    n ^= (b & 0x7f) << 21;
                    if (b > 0x7f) {
                        b = read();
                        n ^= (b & 0x7f) << 28;
                        if (b > 0x7f) {
                            throw new IOException("Invalid int encoding");
                        }
                    }
                }
            }
        }
        return (n >>> 1) ^ -(n & 1); // back to two's-complement
    }

    @Override
    public long readLong() throws IOException
    {
        int b = read();
        int n = b & 0x7f;
        long l;
        if (b > 0x7f) {
            b = read();
            n ^= (b & 0x7f) << 7;
            if (b > 0x7f) {
                b = read();
                n ^= (b & 0x7f) << 14;
                if (b > 0x7f) {
                    b = read();
                    n ^= (b & 0x7f) << 21;
                    if (b > 0x7f) {
                        // only the low 28 bits can be set, so this won't carry
                        // the sign bit to the long
                        l = innerLongDecode((long)n);
                    } else {
                        l = n;
                    }
                } else {
                    l = n;
                }
            } else {
                l = n;
            }
        } else {
            l = n;
        }
        return (l >>> 1) ^ -(l & 1); // back to two's-complement
    }

    // splitting readLong up makes it faster because of the JVM does more
    // optimizations on small methods
    private long innerLongDecode(long l) throws IOException
    {
        int b = read();
        l ^= (b & 0x7fL) << 28;
        if (b > 0x7f) {
            b = read();
            l ^= (b & 0x7fL) << 35;
            if (b > 0x7f) {
                b = read();
                l ^= (b & 0x7fL) << 42;
                if (b > 0x7f) {
                    b = read();
                    l ^= (b & 0x7fL) << 49;
                    if (b > 0x7f) {
                        b = read();
                        l ^= (b & 0x7fL) << 56;
                        if (b > 0x7f) {
                            b = read();
                            l ^= (b & 0x7fL) << 63;
                            if (b > 0x7f) {
                                throw new IOException("Invalid long encoding");
                            }
                        }
                    }
                }
            }
        }
        return l;
    }

    @Override
    public float readFloat() throws IOException
    {
        int n = read() | (read() << 8) | (read() << 16) | (read() << 24);

        return Float.intBitsToFloat(n);
    }

    @Override
    public double readDouble() throws IOException
    {
        int n1 = read() | (read() << 8) | (read() << 16) | (read() << 24);
        int n2 = read() | (read() << 8) | (read() << 16) | (read() << 24);

        return Double.longBitsToDouble((((long) n1) & 0xffffffffL) | (((long) n2) << 32));
    }

    @Override
    public ByteBuffer readBytes(ByteBuffer old) throws IOException
    {
        int length = readInt();
        if (length > buffer_.remaining()) {
            throw new EOFException();
        }
        ByteBuffer slice = buffer_.slice();
        slice.limit(length);
        return slice;
    }

    @Override
    protected void doSkipBytes(long length) throws IOException
    {
        if (length > buffer_.remaining()) {
            throw new EOFException();
        }
        buffer_.position(buffer_.position() + (int) length);
    }

    @Override
    protected void doReadBytes(byte[] bytes, int start, int length) throws IOException
    {
        try {
            buffer_.get(bytes, start, length);
        } catch (BufferUnderflowException e) {
            throw new EOFException();
        }
    }

    @Override
    public boolean isEnd() throws IOException
    {
        return buffer_.hasRemaining();
    }

    @Override
    public InputStream inputStream()
    {
        List<ByteBuffer> buffers = Lists.newArrayList(buffer_);
        return new ByteBufferInputStream(buffers);
    }

    private ByteBuffer buffer_;

    private final static ByteBuffer EMPTY_BUFFER = ByteBuffer.allocate(0);
}
