/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Objects;
import com.google.common.collect.Maps;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

/**
 * @author Chunzhao Zheng
 */
public class PropertyScope implements Reusable
{
    public static class Serializer implements JsonSerializer<PropertyScope>
    {
        @Override
        public JsonElement serialize(PropertyScope scope, Type type, JsonSerializationContext context)
        {
            JsonObject object = new JsonObject();
            for (ScopeType st : scope.getScopeTypes()) {
                String value = scope.getScopeValue(st);
                object.addProperty(st.alias(), value);
            }
            return object;
        }
    }

    public static class Deserializer implements JsonDeserializer<PropertyScope>
    {
        @Override
        public PropertyScope deserialize(JsonElement json, Type type, JsonDeserializationContext context)
        {
            PropertyScope scope = new PropertyScope();
            if (json.isJsonObject()) {
                JsonObject object = json.getAsJsonObject();
                for (ScopeType st : ScopeType.values()) {
                    if (st == ScopeType.DEFAULT) {
                        continue;
                    }
                    JsonElement element = object.get(st.alias());
                    if (element != null) {
                        String value = element.getAsString();
                        scope.addScope(st, value);
                    }
                }
            }
            return scope;
        }
    }

    final static ObjectFactory<PropertyScope> FACTORY = new ObjectFactory<PropertyScope>()
    {
        @Override
        protected PropertyScope create()
        {
            return new PropertyScope();
        }
    };

    PropertyScope()
    {
        scopes_ = Maps.newHashMap();
    }

    @Override
    public void reset()
    {
        scopes_.clear();
    }

    public static PropertyScope create()
    {
        return FACTORY.object();
    }

    public void addScope(ScopeType type, String value)
    {
        scopes_.put(type, value);
    }
    
    public boolean isEmpty() 
    {
    	return scopes_.isEmpty();
    }
    
    public int size() {
    	return scopes_.size();
    }

    public Set<ScopeType> getScopeTypes()
    {
        return scopes_.keySet();
    }

    public String getScopeValue(ScopeType type)
    {
        return scopes_.get(type);
    }

    public String toJson()
    {
        return GSON.toJson(this);
    }

    @Override
    public String toString()
    {
        Objects.ToStringHelper helper = Objects.toStringHelper(PropertyScope.class).omitNullValues();
        for (Map.Entry<ScopeType, String> scope : scopes_.entrySet()) {
            helper.add(scope.getKey().alias(), scope.getValue());
        }
        return helper.toString();
    }

    private final Map<ScopeType, String> scopes_;

    private final static Gson GSON;
    static
    {
        GSON = new GsonBuilder().setPrettyPrinting()
                                .registerTypeAdapter(PropertyScope.class, new PropertyScope.Serializer())
                                .registerTypeAdapter(PropertyScope.class, new PropertyScope.Deserializer())
                                .create();
    }
}
