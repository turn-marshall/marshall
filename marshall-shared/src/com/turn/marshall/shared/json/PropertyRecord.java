/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared.json;

import com.google.gson.annotations.SerializedName;
import com.turn.marshall.shared.EntityStatus;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

/**
 * @author Chunzhao Zheng
 */
public class PropertyRecord implements Reusable
{
    final static ObjectFactory<PropertyRecord> FACTORY = new ObjectFactory<PropertyRecord>()
    {
        @Override
        protected PropertyRecord create()
        {
            return new PropertyRecord();
        }
    };

    PropertyRecord() {}

    @Override
    public void reset()
    {
        key_ = null;
        value_ = null;
    }

    public static PropertyRecord create()
    {
        return FACTORY.object();
    }

    public static PropertyRecord create(String key, Long groupId, String value, Long scopeId, Long entityId, EntityStatus entityStatus)
    {
        PropertyRecord record = FACTORY.object();
        record.setKey(key);
        record.setGroupId(groupId);
        record.setValue(value);
        record.setScopeId(scopeId);
        record.setEntityId(entityId);
        record.setEntityStatus(entityStatus);
        return record;
    }

    @SerializedName("groupId")
    Long groupId_;

    @SerializedName("key")
    String key_;

    @SerializedName("value")
    String value_;
    
    @SerializedName("scopeId")
    Long scopeId_;
    
    @SerializedName("entityId")
    Long entityId_;

    @SerializedName("entityStatus")
    EntityStatus entityStatus_;

    @SerializedName("reason")
    String reason_;
    
	public Long getGroupId() {
		return groupId_;
	}

	public void setGroupId(Long groupId) {
		this.groupId_ = groupId;
	}

	public String getKey() {
		return key_;
	}

	public void setKey(String key) {
		this.key_ = key;
	}

	public String getValue() {
		return value_;
	}

	public void setValue(String value) {
		this.value_ = value;
	}

	public Long getScopeId() {
		return scopeId_;
	}

	public void setScopeId(Long scopeId) {
		this.scopeId_ = scopeId;
	}

	public Long getEntityId() {
		return entityId_;
	}

	public void setEntityId(Long entityId) {
		this.entityId_ = entityId;
	}

	public EntityStatus getEntityStatus() {
		return entityStatus_;
	}

	public void setEntityStatus(EntityStatus entityStatus) {
		this.entityStatus_ = entityStatus;
	}
	
	public String getReason() {
		return reason_;
	}

	public void setReason(String reason) {
		this.reason_ = reason;
	}

}
