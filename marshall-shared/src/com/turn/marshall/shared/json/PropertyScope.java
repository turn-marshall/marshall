package com.turn.marshall.shared.json;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

import com.google.gson.annotations.SerializedName;

public class PropertyScope implements Reusable{
    final static ObjectFactory<PropertyScope> FACTORY = new ObjectFactory<PropertyScope>()
    {
        @Override
        protected PropertyScope create()
        {
            return new PropertyScope();
        }
    };

    PropertyScope() {}

    @Override
    public void reset()
    {
        scopeId_ = -1;
        scopeName_ = null;
    }

    public static PropertyScope create()
    {
        return FACTORY.object();
    }

    public static PropertyScope create(long scopeId, String scopeName)
    {
    	PropertyScope scope = FACTORY.object();
    	scope.setScopeId(scopeId);
    	scope.setScopeName(scopeName);
        return scope;
    }


    public long getScopeId() {
		return scopeId_;
	}

	public void setScopeId(long scopeId) {
		this.scopeId_ = scopeId;
	}

	public String getScopeName() {
		return scopeName_;
	}

	public void setScopeName(String scopeName) {
		this.scopeName_ = scopeName;
	}


	@SerializedName("scopeId")
    long scopeId_;

    @SerializedName("scopeName")
    String scopeName_;
}