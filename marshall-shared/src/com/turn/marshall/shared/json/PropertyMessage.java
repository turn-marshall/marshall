/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared.json;

import java.util.List;

import com.google.common.collect.Lists;

import com.google.gson.annotations.SerializedName;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

/**
 * @author Chunzhao Zheng
 */
public class PropertyMessage implements Reusable
{
    final static ObjectFactory<PropertyMessage> FACTORY = new ObjectFactory<PropertyMessage>()
    {
        @Override
        protected PropertyMessage create()
        {
            return new PropertyMessage();
        }
    };

    PropertyMessage()
    {
        records_ = Lists.newArrayList();
    }

    public static PropertyMessage create()
    {
        return FACTORY.object();
    }

    public static PropertyMessage create(List<PropertyRecord> records)
    {
        PropertyMessage message = FACTORY.object();
        for (PropertyRecord record : records) {
            message.addRecord(record);
        }
        return message;
    }

    @Override
    public void reset()
    {
        records_.clear();
    }

    public void addRecord(PropertyRecord record)
    {
        records_.add(record);
    }

    public List<PropertyRecord> getRecords()
    {
        return records_;
    }

    @SerializedName("records")
    private final List<PropertyRecord> records_;
}
