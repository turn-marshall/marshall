package com.turn.marshall.shared.json;

import java.util.Date;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

import com.google.gson.annotations.SerializedName;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.UserAction;


public class PropertyAudit implements Reusable{
    final static ObjectFactory<PropertyAudit> FACTORY = new ObjectFactory<PropertyAudit>()
    {
        @Override
        protected PropertyAudit create()
        {
            return new PropertyAudit();
        }
    };

    PropertyAudit() {}

    @Override
    public void reset()
    {
        auditId_ = null;
    }

    public Long getAuditId() {
		return auditId_;
	}

	public void setAuditId(Long auditId) {
		this.auditId_ = auditId;
	}

	public Long getScopeId() {
		return scopeId_;
	}

	public void setScopeId(Long scopeId) {
		this.scopeId_ = scopeId;
	}

	public Long getGroupId() {
		return groupId_;
	}

	public void setGroupId(Long groupId) {
		this.groupId_ = groupId;
	}

	public String getKey() {
		return key_;
	}

	public void setKey(String key) {
		this.key_ = key;
	}

	public String getValue() {
		return value_;
	}

	public void setValue(String value) {
		this.value_ = value;
	}

	public EntityStatus getEntityStatus() {
		return entityStatus_;
	}

	public void setEntityStatus(EntityStatus entityStatus) {
		this.entityStatus_ = entityStatus;
	}

	public Long getEntityVersion() {
		return entityVersion_;
	}

	public void setEntityVersion(Long entityVersion) {
		this.entityVersion_ = entityVersion;
	}

	public String getCommittedBy() {
		return committedBy_;
	}

	public void setCommittedBy(String committedBy) {
		this.committedBy_ = committedBy;
	}

	public Date getCommitTime() {
		return commitTime_;
	}

	public void setCommitTime(Date commitTime) {
		this.commitTime_ = commitTime;
	}

	public String getCommitReason() {
		return commitReason_;
	}

	public void setCommitReason(String commitReason) {
		this.commitReason_ = commitReason;
	}

	public UserAction getUserAction() {
		return userAction_;
	}

	public void setUserAction(UserAction userAction) {
		this.userAction_ = userAction;
	}

	public static PropertyAudit create()
    {
        return FACTORY.object();
    }

    public static PropertyAudit create(Long AuditId, Long ScopeId, Long GroupId, String Key, String Value, 
    		EntityStatus entityStatus, Long entityVersion, String committedBy, String commitReason, UserAction userAction, Date dt)
    {
    	PropertyAudit Audit = FACTORY.object();
    	Audit.setAuditId(AuditId);
    	Audit.setScopeId(ScopeId);
    	Audit.setGroupId(GroupId);
    	Audit.setKey(Key);
    	Audit.setValue(Value);
    	Audit.setEntityStatus(entityStatus);
    	Audit.setEntityVersion(entityVersion);
    	Audit.setCommittedBy(committedBy);
    	Audit.setCommitReason(commitReason);
    	Audit.setCommitTime(dt);
    	Audit.setUserAction(userAction);
        return Audit;
    }

	@SerializedName("AuditId")
    Long auditId_;
	
	@SerializedName("ScopeId")
	Long scopeId_;

	@SerializedName("GroupId")
	Long groupId_;

	@SerializedName("Key")
	String key_;

	@SerializedName("Value")
	String value_;
	
	@SerializedName("EntityStatus")
	EntityStatus entityStatus_;
	
	@SerializedName("EntityVersion")
	Long entityVersion_;
	
	@SerializedName("CommittedBy")
	String committedBy_;
	
	@SerializedName("CommitTime")
	Date commitTime_;
	
	@SerializedName("CommitReason")
	String commitReason_;
	
	@SerializedName("UserAction")
	UserAction userAction_;
    
}