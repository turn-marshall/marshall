package com.turn.marshall.shared.json;

import javolution.context.ObjectFactory;
import javolution.lang.Reusable;

import com.google.gson.annotations.SerializedName;

public class PropertyGroup implements Reusable{
    final static ObjectFactory<PropertyGroup> FACTORY = new ObjectFactory<PropertyGroup>()
    {
        @Override
        protected PropertyGroup create()
        {
            return new PropertyGroup();
        }
    };

    PropertyGroup() {}

    @Override
    public void reset()
    {
        groupId_ = -1;
        groupName_ = null;
    }

    public static PropertyGroup create()
    {
        return FACTORY.object();
    }

    public static PropertyGroup create(long groupId, String groupName)
    {
    	PropertyGroup group = FACTORY.object();
    	group.setGroupId(groupId);
    	group.setGroupName(groupName);
        return group;
    }


    public long getGroupId() {
		return groupId_;
	}

	public void setGroupId(long groupId) {
		this.groupId_ = groupId;
	}

	public String getGroupName() {
		return groupName_;
	}

	public void setGroupName(String groupName) {
		this.groupName_ = groupName;
	}


	@SerializedName("groupId")
    long groupId_;

    @SerializedName("groupName")
    String groupName_;
}