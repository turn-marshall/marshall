/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared;

import static org.testng.Assert.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author Chunzhao Zheng
 */
@Test(groups = { "unittest" })
public class PropertyScopeTest
{
    private static Gson gson;

    @BeforeClass
    public static void initialize()
    {
        GsonBuilder builder = new GsonBuilder();
        gson = builder.registerTypeAdapter(PropertyScope.class, new PropertyScope.Serializer())
                      .registerTypeAdapter(PropertyScope.class, new PropertyScope.Deserializer())
                      .setPrettyPrinting()
                      .create();
    }

    @Test
    public void deserializePropertyScope()
    {
        PropertyScope scope = PropertyScope.create();
        scope.addScope(ScopeType.DATA_CENTER, "ds-1");
        scope.addScope(ScopeType.HOST_NAME, "host-1");
        String text = gson.toJson(scope);

        PropertyScope another = gson.fromJson(text, PropertyScope.class);
        assertNotNull(another);
        assertEquals(another.getScopeTypes(), scope.getScopeTypes());

        for (ScopeType type : scope.getScopeTypes()) {
            assertEquals(another.getScopeValue(type), scope.getScopeValue(type));
        }
    }

    @Test
    public void serializePropertyScope()
    {
        PropertyScope scope = PropertyScope.create();
        for (ScopeType type : ScopeType.values()) {
            if (type != ScopeType.DEFAULT) {
                scope.addScope(type, type.alias() + "-value");
            }
        }
        String text = gson.toJson(scope);
        assertNotNull(text);

        PropertyScope another = gson.fromJson(text, PropertyScope.class);
        assertNotNull(another);
        assertEquals(another.getScopeTypes(), scope.getScopeTypes());

        for (ScopeType type : scope.getScopeTypes()) {
            assertEquals(another.getScopeValue(type), scope.getScopeValue(type));
        }
    }
}
