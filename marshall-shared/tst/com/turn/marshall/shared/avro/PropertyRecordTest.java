/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared.avro;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.nio.ByteBuffer;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.turn.marshall.shared.avro.generated.PropertyRecord;

/**
 * @author Chunzhao Zheng
 */
@Test(groups = { "unittest" })
public class PropertyRecordTest
{
    private static ByteBufferAdapter<PropertyRecord> adapter;

    @BeforeClass
    public static void initialize()
    {
        adapter = new ByteBufferAdapter<PropertyRecord>(PropertyRecord.class);
    }

    @Test
    public void deserializePropertyRecord()
    {
        PropertyRecord record = new PropertyRecord("name-1", "group-1", "value-1");

        ByteBuffer buffer = adapter.toByteBuffer(record);
        assertNotNull(buffer);

        PropertyRecord another = adapter.fromByteBuffer(buffer);
        assertNotNull(another);
        assertEquals(another, record);
    }

    @Test
    public void serializePropertyRecord()
    {
        PropertyRecord record = new PropertyRecord("name-1", "group-1", "value-1");

        ByteBuffer buffer = adapter.toByteBuffer(record);
        assertNotNull(buffer);

        PropertyRecord another = adapter.fromByteBuffer(buffer);
        assertNotNull(another);
        assertEquals(another, record);
    }
}
