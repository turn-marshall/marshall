/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.shared.avro;

import static org.testng.Assert.*;
import static org.testng.Assert.assertNotNull;

import java.nio.ByteBuffer;
import java.util.List;

import com.google.common.collect.Lists;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.turn.marshall.shared.avro.generated.PropertyMessage;
import com.turn.marshall.shared.avro.generated.PropertyRecord;

/**
 * @author Chunzhao Zheng
 */
@Test(groups = { "unittest" })
public class PropertyMessageTest
{
    private static ByteBufferAdapter<PropertyMessage> adapter;

    @BeforeClass
    public static void initialize()
    {
        adapter = new ByteBufferAdapter<PropertyMessage>(PropertyMessage.class);
    }

    @Test
    public void deserializePropertyMessage()
    {
        PropertyRecord record1 = new PropertyRecord("name-1", "group-1", "value-1");
        PropertyRecord record2 = new PropertyRecord("name-2", "group-2", "value-2");
        List<PropertyRecord> records = Lists.newArrayList(record1, record2);
        PropertyMessage message = new PropertyMessage(records);

        ByteBuffer buffer = adapter.toByteBuffer(message);
        assertNotNull(buffer);

        PropertyMessage another = adapter.fromByteBuffer(buffer);
        assertNotNull(another);
        assertEquals(another, message);
        assertNotNull(another.getRecords());
        assertEquals(another.getRecords().size(), 2);
        assertEquals(another.getRecords().get(0), record1);
        assertEquals(another.getRecords().get(1), record2);
    }

    @Test
    public void serializePropertyMessage()
    {
        PropertyRecord record1 = new PropertyRecord("name-1", "group-1", "value-1");
        PropertyRecord record2 = new PropertyRecord("name-2", "group-2", "value-2");
        List<PropertyRecord> records = Lists.newArrayList(record1, record2);
        PropertyMessage message = new PropertyMessage(records);

        ByteBuffer buffer = adapter.toByteBuffer(message);
        assertNotNull(buffer);

        PropertyMessage another = adapter.fromByteBuffer(buffer);
        assertNotNull(another);
        assertEquals(another, message);
        assertNotNull(another.getRecords());
        assertEquals(another.getRecords().size(), 2);
        assertEquals(another.getRecords().get(0), record1);
        assertEquals(another.getRecords().get(1), record2);
    }
}
