///**
// * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
// * Proprietary and confidential.
// */
//package com.turn.marshall.shared.json;
//
//import static org.testng.Assert.*;
//
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
///**
// * @author Chunzhao Zheng
// */
//@Test(groups = { "unittest" })
//public class PropertyRecordTest
//{
//    private static Gson gson;
//
//    @BeforeClass
//    public static void initialize()
//    {
//        GsonBuilder builder = new GsonBuilder();
//        gson = builder.setPrettyPrinting().create();
//    }
//
//    @Test
//    public void deserializePropertyRecord()
//    {
//        PropertyRecord record = PropertyRecord.create("name-1", "group-1", "value-1");
//        String text = gson.toJson(record);
//
//        PropertyRecord another = gson.fromJson(text, PropertyRecord.class);
//        assertNotNull(another);
//        assertEquals(another.getName(), record.getName());
//        assertEquals(another.getGroup(), record.getGroup());
//        assertEquals(another.getValue(), record.getValue());
//    }
//
//    @Test
//    public void serializePropertyRecord()
//    {
//        PropertyRecord record = PropertyRecord.create("name-1", "group-1", "value-1");
//        String text = gson.toJson(record);
//        assertNotNull(text);
//    }
//}
