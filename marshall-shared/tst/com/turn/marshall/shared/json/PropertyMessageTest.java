///**
// * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
// * Proprietary and confidential.
// */
//package com.turn.marshall.shared.json;
//
//import static org.testng.Assert.*;
//
//import java.util.List;
//
//import com.google.common.collect.Lists;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
///**
// * @author Chunzhao Zheng
// */
//@Test(groups = { "unittest" })
//public class PropertyMessageTest
//{
//    private static Gson gson;
//
//    @BeforeClass
//    public static void initialize()
//    {
//        GsonBuilder builder = new GsonBuilder();
//        gson = builder.setPrettyPrinting().create();
//    }
//
//    @Test
//    public void deserializePropertyMessage()
//    {
//        PropertyRecord record1 = PropertyRecord.create("name-1", "group-1", "value-1");
//        PropertyRecord record2 = PropertyRecord.create("name-2", "group-2", "value-2");
//        List<PropertyRecord> records = Lists.newArrayList(record1, record2);
//        PropertyMessage message = PropertyMessage.create(records);
//
//        String text = gson.toJson(message);
//        assertNotNull(text);
//
//        PropertyMessage another = gson.fromJson(text, PropertyMessage.class);
//        assertNotNull(another);
//        assertNotNull(another.getRecords());
//        assertEquals(another.getRecords().size(), 2);
//
//        PropertyRecord another1 = another.getRecords().get(0);
//        assertEquals(another1.getName(), record1.getName());
//        assertEquals(another1.getGroup(), record1.getGroup());
//        assertEquals(another1.getValue(), record1.getValue());
//
//        PropertyRecord another2 = another.getRecords().get(1);
//        assertEquals(another2.getName(), record2.getName());
//        assertEquals(another2.getGroup(), record2.getGroup());
//        assertEquals(another2.getValue(), record2.getValue());
//    }
//
//    @Test
//    public void serializePropertyMessage()
//    {
//        PropertyRecord record = PropertyRecord.create("name-1", "group-1", "value-1");
//        List<PropertyRecord> records = Lists.newArrayList(record);
//        PropertyMessage message = PropertyMessage.create(records);
//
//        String text = gson.toJson(message);
//        assertNotNull(text);
//
//        PropertyMessage another = gson.fromJson(text, PropertyMessage.class);
//        assertNotNull(another);
//        assertNotNull(another.getRecords());
//        assertEquals(another.getRecords().size(), 1);
//    }
//}
