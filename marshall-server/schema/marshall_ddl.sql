-- Drop all entities
DROP TABLE IF EXISTS app_property CASCADE;
DROP TABLE IF EXISTS app_property_audit CASCADE;
DROP TABLE IF EXISTS app_property_scope CASCADE;
DROP TABLE IF EXISTS app_property_group CASCADE;
DROP TABLE IF EXISTS app_property_group_audit CASCADE;
DROP TABLE IF EXISTS app_property_scope_audit CASCADE;
DROP TABLE IF EXISTS CARBONADO_SEQUENCE CASCADE;

-- Create entities from scratch
-- Defines server type
CREATE TABLE APP_PROPERTY_GROUP (
    property_group_id INT NOT NULL,
    property_group VARCHAR(500) NOT NULL,
    entity_status INT NOT NULL DEFAULT -1,
    PRIMARY KEY(property_group_id)
);

-- Defines hardware allocation
CREATE TABLE APP_PROPERTY_SCOPE (
    property_scope_id INT NOT NULL,
    property_scope_type INT NOT NULL,
    property_scope VARCHAR(200) NOT NULL,
    entity_status INT NOT NULL DEFAULT -1,
    PRIMARY KEY(property_scope_id)
);

-- Tracks and versions changes to groups
CREATE TABLE APP_PROPERTY_GROUP_AUDIT (
    property_group_audit_id INT NOT NULL,
    property_group_id INT NOT NULL,
    property_group VARCHAR(200) NOT NULL,
    entity_status INT NOT NULL DEFAULT -1,
    entity_version INT NOT NULL,
    committed_by VARCHAR(200) NOT NULL,
    committed_date DATE NOT NULL,
    committed_reason VARCHAR(1000) NOT NULL,
    user_action INT NOT NULL,
    PRIMARY KEY(property_group_audit_id),
    CONSTRAINT FK2_app_property_group_audit FOREIGN KEY(property_group_id)
        REFERENCES app_property_group(property_group_id)
);

-- Tracks and versions changes to scopes
CREATE TABLE APP_PROPERTY_SCOPE_AUDIT (
    property_scope_audit_id INT NOT NULL,
    property_scope_id INT NOT NULL,
    property_scope VARCHAR(200) NOT NULL,
    property_scope_type INT NOT NULL,
    entity_status INT NOT NULL DEFAULT -1,
    entity_version INT NOT NULL,
    committed_by VARCHAR(200) NOT NULL,
    committed_date DATE NOT NULL,
    committed_reason VARCHAR(1000) NOT NULL,
    user_action INT NOT NULL,
    PRIMARY KEY(property_scope_audit_id),
    CONSTRAINT FK1_app_property_scope_audit FOREIGN KEY(property_scope_id)
        REFERENCES app_property_scope(property_scope_id)
);

-- Defines a configuration property
CREATE TABLE APP_PROPERTY (
    property_id INT NOT NULL,
    property_scope_id INT NULL,
    property_group_id INT NULL,
    property_key VARCHAR(200) NOT NULL,
    property_value VARCHAR(1000) NOT NULL,
    entity_status INT NOT NULL DEFAULT -1,
    PRIMARY KEY(property_id),
    CONSTRAINT FKpro_pro_9193185106269072477 FOREIGN KEY(property_scope_id)
        REFERENCES app_property_scope(property_scope_id),
    CONSTRAINT FKpro_pro_9193185153429830985 FOREIGN KEY(property_group_id)
        REFERENCES app_property_group(property_group_id),
    CONSTRAINT app_property_unique1 UNIQUE(property_key, property_group_id, property_scope_id)
);

CREATE INDEX FK1_app_property ON app_property(property_group_id);
CREATE INDEX FK2_app_property ON app_property(property_scope_id);

-- Tracks and versions changes to property configuration changes 
CREATE TABLE APP_PROPERTY_AUDIT (
    property_audit_id INT NOT NULL,
    property_id INT NOT NULL,
    property_scope_id INT NULL,
    property_group_id INT NULL,
    property_key VARCHAR(200) NOT NULL,
    property_value VARCHAR(1000) NOT NULL,
    entity_status INT NOT NULL DEFAULT -1,
    entity_version INT NOT NULL,
    committed_by VARCHAR(200) NOT NULL,
    committed_date DATE NOT NULL,
    committed_reason VARCHAR(1000) NOT NULL,
    user_action INT NOT NULL,
    PRIMARY KEY(property_audit_id),
    CONSTRAINT FK1_app_property_audit FOREIGN KEY(property_id)
        REFERENCES app_property(property_id)
);

CREATE INDEX FK1_app_property_audit ON app_property_audit(property_group_id);
CREATE INDEX FK2_app_property_audit ON app_property_audit(property_group_id);

CREATE TABLE CARBONADO_SEQUENCE (
     NAME VARCHAR(100) PRIMARY KEY,
     INITIAL_VALUE BIGINT NOT NULL,
     NEXT_VALUE BIGINT NOT NULL,
     VERSION INT NOT NULL
);


