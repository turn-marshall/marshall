package com.turn.marshall.guice;

import com.amazon.carbonado.Repository;
import com.amazon.carbonado.RepositoryException;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import com.turn.marshall.shared.AppStage;
import com.turn.marshall.storage.StorageRepository;

/**
 * @author Chunzhao Zheng
 */
public class MarshallStorageModule extends AbstractModule
{
    public MarshallStorageModule(AppStage stage, StorageRepository repository)
    {
        stage_ = stage;
        repository_ = repository;
    }

    @Override
    protected void configure()
    {
    }

    @Provides @Singleton @Marshall
    Repository createCabonadoRepository() throws RepositoryException
    {
        return repository_.repository(stage_, REPOSITORY_NAME);
    }

    private final AppStage stage_;
    private final StorageRepository repository_;

    private final static String REPOSITORY_NAME = "marshall";
}