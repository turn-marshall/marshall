/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Stage;
import com.google.inject.servlet.GuiceServletContextListener;

import com.turn.marshall.shared.AppStage;
import com.turn.marshall.storage.StorageRepository;

/**
 * @author Chunzhao Zheng
 */
public class MarshallContextDebugger extends GuiceServletContextListener
{
    @Override
    protected Injector getInjector()
    {
        return Guice.createInjector(Stage.PRODUCTION, modules());
    }

    protected Module[] modules()
    {
        return new Module[] {
                new MarshallEndpointModule(),
                new MarshallSharedModule(),
                new MarshallStorageModule(AppStage.DEVELOPMENT, StorageRepository.BDB)
        };
    }
}
