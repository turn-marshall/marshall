/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.guice;

import com.google.sitebricks.SitebricksModule;

import com.turn.marshall.endpoint.ServiceEndpointDefinition;

/**
 * @author Chunzhao Zheng
 */
public class MarshallEndpointModule extends SitebricksModule
{
    @Override
    protected void configureSitebricks()
    {
        for (ServiceEndpointDefinition definition : ServiceEndpointDefinition.values()) {
            at(BASE_PATH + definition.path()).serve(definition.service());
        }
    }

    private static final String BASE_PATH = "/marshall/api/";
}
