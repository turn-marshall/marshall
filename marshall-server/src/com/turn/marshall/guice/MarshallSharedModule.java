/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.guice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import com.turn.marshall.shared.PropertyScope;
import com.turn.marshall.shared.avro.ByteBufferAdapter;
import com.turn.marshall.shared.avro.generated.PropertyMessage;
import com.turn.marshall.shared.avro.generated.PropertyRecord;

/**
 * @author Chunzhao Zheng
 */
public class MarshallSharedModule extends AbstractModule
{
    @Override
    protected void configure()
    {
    }

    @Provides @Singleton @Marshall
    Gson configureGson()
    {
        GsonBuilder builder = new GsonBuilder();
        return builder.registerTypeAdapter(PropertyScope.class, new PropertyScope.Serializer())
                      .registerTypeAdapter(PropertyScope.class, new PropertyScope.Deserializer())
                      .setPrettyPrinting()
                      .create();
    }

    @Provides @Singleton
    ByteBufferAdapter<PropertyMessage> createPropertyMessageAdapter()
    {
        return new ByteBufferAdapter<PropertyMessage>(PropertyMessage.class);
    }

    @Provides @Singleton
    ByteBufferAdapter<PropertyRecord> createPropertyRecordAdapter()
    {
        return new ByteBufferAdapter<PropertyRecord>(PropertyRecord.class);
    }
}
