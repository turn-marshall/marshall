package com.turn.marshall.model;

import com.amazon.carbonado.Alias;
import com.amazon.carbonado.FetchException;
import com.amazon.carbonado.Join;
import com.amazon.carbonado.PrimaryKey;
import com.amazon.carbonado.Sequence;

import com.turn.marshall.model.adapter.ScopeTypeAdapter;
import com.turn.marshall.shared.ScopeType;

/**
 * @author Chunzhao Zheng
 */
@PrimaryKey("propertyScopeAuditId")
@Alias("app_property_scope_audit")
public abstract class PropertyScopeAudit implements Auditable<PropertyScopeEntity, PropertyScopeAudit>
{
    @Sequence("PROPERTY_SCOPE_AUDIT_SEQUENCE")
    public abstract long getPropertyScopeAuditId();
    public abstract void setPropertyScopeAuditId(long pk);

    public abstract long getPropertyScopeId();
    public abstract void setPropertyScopeId(long id);

    public abstract String getPropertyScope();
    public abstract void setPropertyScope(String scope);

    @ScopeTypeAdapter
    public abstract ScopeType getPropertyScopeType();
    public abstract void setPropertyScopeType(ScopeType type);

    @Join(internal = "propertyScopeId", external = "propertyScopeId")
    public abstract PropertyScopeEntity getPropertyScopeEntity() throws FetchException;
    public abstract void setPropertyScopeEntity(PropertyScopeEntity entity);

    @Override
    public void copyTo(PropertyScopeEntity entity)
    {
        entity.setPropertyScope(getPropertyScope());
        entity.setPropertyScopeType(getPropertyScopeType());
    }

    @Override
    public PropertyScopeAudit fillFrom(PropertyScopeEntity entity)
    {
        setPropertyScopeId(entity.getPropertyScopeId());
        setPropertyScope(entity.getPropertyScope());
        setPropertyScopeType(entity.getPropertyScopeType());

        return this;
    }
}
