package com.turn.marshall.model;

import com.amazon.carbonado.Alias;
import com.amazon.carbonado.AlternateKeys;
import com.amazon.carbonado.FetchException;
import com.amazon.carbonado.Index;
import com.amazon.carbonado.Indexes;
import com.amazon.carbonado.Join;
import com.amazon.carbonado.Key;
import com.amazon.carbonado.Nullable;
import com.amazon.carbonado.PrimaryKey;
import com.amazon.carbonado.Query;
import com.amazon.carbonado.Sequence;

/**
 * @author Chunzhao Zheng
 */
@PrimaryKey("propertyId")
@AlternateKeys(@Key({"propertyKey", "propertyGroupId", "propertyScopeId"}))
@Indexes(@Index("propertyKey"))
@Alias("app_property")
public interface PropertyEntity extends Entity<PropertyEntity>
{
    @Sequence("PROPERTY_SEQUENCE")
    public long getPropertyId();
    public void setPropertyId(long pk);

    public String getPropertyKey();
    public void setPropertyKey(String key);

    public String getPropertyValue();
    public void setPropertyValue(String value);

    @Nullable
    public Long getPropertyGroupId();
    public void setPropertyGroupId(Long id);

    @Nullable
    public Long getPropertyScopeId();
    public void setPropertyScopeId(Long id);

    @Nullable
    @Join(internal = "propertyGroupId", external = "propertyGroupId")
    public PropertyGroupEntity getPropertyGroup() throws FetchException;
    public void setPropertyGroup(PropertyGroupEntity group);

    @Nullable
    @Join(internal = "propertyScopeId", external = "propertyScopeId")
    public PropertyScopeEntity getPropertyScope() throws FetchException;
    public void setPropertyScope(PropertyScopeEntity scope);

    @Join(internal = "propertyId", external = "propertyId")
    public Query<PropertyAudit> getPropertyAudits() throws FetchException;
}
