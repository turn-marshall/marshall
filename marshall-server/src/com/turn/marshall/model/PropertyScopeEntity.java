package com.turn.marshall.model;

import com.amazon.carbonado.Alias;
import com.amazon.carbonado.AlternateKeys;
import com.amazon.carbonado.FetchException;
import com.amazon.carbonado.Join;
import com.amazon.carbonado.Key;
import com.amazon.carbonado.PrimaryKey;
import com.amazon.carbonado.Query;
import com.amazon.carbonado.Sequence;

import com.turn.marshall.model.adapter.ScopeTypeAdapter;
import com.turn.marshall.shared.ScopeType;

/**
 * @author Chunzhao Zheng
 */
@PrimaryKey("propertyScopeId")
@AlternateKeys(@Key("propertyScopeType"))
@Alias("app_property_scope")
public interface PropertyScopeEntity extends Entity<PropertyScopeEntity>
{
    @Sequence("PROPERTY_SCOPE_SEQUENCE")
    public long getPropertyScopeId();
    public void setPropertyScopeId(long pk);

    @ScopeTypeAdapter
    public ScopeType getPropertyScopeType();
    public void setPropertyScopeType(ScopeType type);

    public String getPropertyScope();
    public void setPropertyScope(String value);

    @Join(internal = "propertyScopeId", external = "propertyScopeId")
    public Query<PropertyEntity> getPropertyEntities() throws FetchException;
}
