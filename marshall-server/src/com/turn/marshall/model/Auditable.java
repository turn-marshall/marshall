package com.turn.marshall.model;

import com.amazon.carbonado.Automatic;
import com.amazon.carbonado.Sequence;
import com.amazon.carbonado.Storable;
import com.amazon.carbonado.adapter.DateTimeAdapter;

import org.joda.time.DateTime;

import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.UserAction;
import com.turn.marshall.model.adapter.EntityStatusAdapter;
import com.turn.marshall.model.adapter.UserActionAdapter;

/**
 * @author Chunzhao Zheng
 */
public interface Auditable<S extends Storable<S>, R extends Storable<R> & Auditable<S, R>> extends Storable<R>
{
    @EntityStatusAdapter
    public EntityStatus getEntityStatus();
    public void setEntityStatus(EntityStatus status);

    @Sequence("ENTITY_VERSION_SEQUENCE")
    public long getEntityVersion();
    public void setEntityVersion(long version);

    public abstract String getCommittedBy();
    public abstract void setCommittedBy(String committer);

    @DateTimeAdapter(timeZone = "UTC")
    public abstract DateTime getCommittedDate();
    public abstract void setCommittedDate(DateTime time);

    public abstract String getCommittedReason();
    public abstract void setCommittedReason(String reason);

    @UserActionAdapter
    public UserAction getUserAction();
    public void setUserAction(UserAction action);

    /**
     * Copies information from this {@link R} instance to an instance of its associated {@link S}.
     *
     * @param entity {@link S} instance to copy the information to
     */
    public void copyTo(S entity);

    /**
     * Fill audit entry with details from source {@link S}.
     *
     * @param entity The source {@link S}
     * @return this
     */
    public R fillFrom(S entity);
}
