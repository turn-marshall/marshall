package com.turn.marshall.model;

import com.amazon.carbonado.Alias;
import com.amazon.carbonado.FetchException;
import com.amazon.carbonado.Join;
import com.amazon.carbonado.Nullable;
import com.amazon.carbonado.PrimaryKey;
import com.amazon.carbonado.Sequence;

/**
 * @author Chunzhao Zheng
 */
@PrimaryKey("propertyAuditId")
@Alias("app_property_audit")
public abstract class PropertyAudit implements Auditable<PropertyEntity, PropertyAudit>
{
    @Sequence("PROPERTY_AUDIT_SEQUENCE")
    public abstract long getPropertyAuditId();
    public abstract void setPropertyAuditId(long pk);

    public abstract long getPropertyId();
    public abstract void setPropertyId(long id);

    public abstract String getPropertyKey();
    public abstract void setPropertyKey(String key);

    public abstract String getPropertyValue();
    public abstract void setPropertyValue(String value);

    @Nullable
    public abstract Long getPropertyGroupId();
    public abstract void setPropertyGroupId(Long id);

    @Nullable
    public abstract Long getPropertyScopeId();
    public abstract void setPropertyScopeId(Long id);

    @Join(internal = "propertyId", external = "propertyId")
    public abstract PropertyEntity getPropertyEntity() throws FetchException;
    public abstract void setPropertyEntity(PropertyEntity entity);

    @Nullable
    @Join(internal = "propertyGroupId", external = "propertyGroupId")
    public abstract PropertyGroupEntity getPropertyGroup() throws FetchException;
    public abstract void setPropertyGroup(PropertyGroupEntity group);

    @Nullable
    @Join(internal = "propertyScopeId", external = "propertyScopeId")
    public abstract PropertyScopeEntity getPropertyScope() throws FetchException;
    public abstract void setPropertyScope(PropertyScopeEntity scope);

    @Override
    public void copyTo(PropertyEntity entity)
    {
        entity.setPropertyKey(getPropertyKey());
        entity.setPropertyValue(getPropertyValue());
    }

    @Override
    public PropertyAudit fillFrom(PropertyEntity entity)
    {
        setPropertyId(entity.getPropertyId());
        setPropertyKey(entity.getPropertyKey());
        setPropertyValue(entity.getPropertyValue());

        return this;
    }
}
