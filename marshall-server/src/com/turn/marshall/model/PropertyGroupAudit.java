/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.model;

import com.amazon.carbonado.Alias;
import com.amazon.carbonado.FetchException;
import com.amazon.carbonado.Join;
import com.amazon.carbonado.PrimaryKey;
import com.amazon.carbonado.Sequence;

/**
 * @author Chunzhao Zheng
 */
@PrimaryKey("propertyGroupAuditId")
@Alias("app_property_group_audit")
public abstract class PropertyGroupAudit implements Auditable<PropertyGroupEntity, PropertyGroupAudit>
{
    @Sequence("PROPERTY_GROUP_AUDIT_SEQUENCE")
    public abstract long getPropertyGroupAuditId();
    public abstract void setPropertyGroupAuditId(long pk);

    public abstract long getPropertyGroupId();
    public abstract void setPropertyGroupId(long id);

    public abstract String getPropertyGroup();
    public abstract void setPropertyGroup(String group);

    @Join(internal = "propertyGroupId", external = "propertyGroupId")
    public abstract PropertyGroupEntity getPropertyGroupEntity() throws FetchException;
    public abstract void setPropertyGroupEntity(PropertyGroupEntity entity);

    @Override
    public void copyTo(PropertyGroupEntity entity)
    {
        entity.setPropertyGroup(getPropertyGroup());
    }

    @Override
    public PropertyGroupAudit fillFrom(PropertyGroupEntity entity)
    {
        setPropertyGroupId(entity.getPropertyGroupId());
        setPropertyGroup(entity.getPropertyGroup());

        return this;
    }
}
