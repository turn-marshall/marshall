/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.model;

import com.amazon.carbonado.Storable;

import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.model.adapter.EntityStatusAdapter;

/**
 * @author Chunzhao Zheng
 */
public interface Entity<S extends Entity<S>> extends Storable<S>
{
    @EntityStatusAdapter
    public EntityStatus getEntityStatus();
    public void setEntityStatus(EntityStatus status);
}
