/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.model;

import com.amazon.carbonado.Alias;
import com.amazon.carbonado.FetchException;
import com.amazon.carbonado.Join;
import com.amazon.carbonado.PrimaryKey;
import com.amazon.carbonado.Query;
import com.amazon.carbonado.Sequence;

/**
 * @author Chunzhao Zheng
 */
@PrimaryKey("propertyGroupId")
@Alias("app_property_group")
public interface PropertyGroupEntity extends Entity<PropertyGroupEntity>
{
    @Sequence("PROPERTY_GROUP_SEQUENCE")
    public long getPropertyGroupId();
    public void setPropertyGroupId(long pk);

    public String getPropertyGroup();
    public void setPropertyGroup(String value);

    @Join(internal = "propertyGroupId", external = "propertyGroupId")
    public Query<PropertyEntity> getPropertyEntities() throws FetchException;
}
