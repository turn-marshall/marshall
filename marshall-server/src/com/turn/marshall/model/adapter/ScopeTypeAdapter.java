/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.model.adapter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.amazon.carbonado.adapter.AdapterDefinition;

import com.turn.marshall.shared.ScopeType;

/**
 * @author Chunzhao Zheng
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@AdapterDefinition(storageTypePreferences = { Integer.class, int.class })
public @interface ScopeTypeAdapter
{
    public static class Adapter
    {
        public Adapter(Class<?> type, String property, ScopeTypeAdapter annotation) {}

        public int adaptToInt(ScopeType scope)
        {
            return scope == null ? -1 : scope.code();
        }

        public Integer adaptToInteger(ScopeType scope)
        {
            return scope == null ? -1 : scope.code();
        }

        public ScopeType adaptToEnum(int code)
        {
            return ScopeType.from(code);
        }

        public ScopeType adaptToEnum(Integer code)
        {
            return code == null ? ScopeType.DEFAULT : ScopeType.from(code);
        }
    }
}
