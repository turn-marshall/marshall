package com.turn.marshall.model.adapter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.amazon.carbonado.adapter.AdapterDefinition;

import com.turn.marshall.shared.UserAction;

/**
 * @author Chunzhao Zheng
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@AdapterDefinition(storageTypePreferences = { Integer.class, int.class })
public @interface UserActionAdapter
{
    public static class Adapter
    {
        public Adapter(Class<?> type, String property, UserActionAdapter annotation) {}

        public int adaptToInt(UserAction action)
        {
            return action == null ? -1 : action.code();
        }

        public Integer adaptToInteger(UserAction action)
        {
            return action == null ? -1 : action.code();
        }

        public UserAction adaptToEnum(int code)
        {
            return UserAction.from(code);
        }

        public UserAction adaptToEnum(Integer code)
        {
            return code == null ? UserAction.UNKNOWN : UserAction.from(code);
        }
    }
}
