package com.turn.marshall.model.adapter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.amazon.carbonado.adapter.AdapterDefinition;

import com.turn.marshall.shared.EntityStatus;

/**
 * @author Chunzhao Zheng
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@AdapterDefinition(storageTypePreferences = { Integer.class, int.class })
public @interface EntityStatusAdapter
{
    public static class Adapter
    {
        public Adapter(Class<?> type, String property, EntityStatusAdapter annotation) {}

        public int adaptToInt(EntityStatus status)
        {
            return status == null ? -1 : status.code();
        }

        public Integer adaptToInteger(EntityStatus status)
        {
            return status == null ? -1 : status.code();
        }

        public EntityStatus adaptToEnum(int code)
        {
            return EntityStatus.from(code);
        }

        public EntityStatus adaptToEnum(Integer code)
        {
            return code == null ? EntityStatus.UNKNOWN : EntityStatus.from(code);
        }
    }
}
