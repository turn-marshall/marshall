/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import com.google.common.io.ByteStreams;
import com.google.inject.TypeLiteral;

import com.turn.marshall.shared.avro.ByteBuffers;

/**
 * @author Chunzhao Zheng
 */
class AvroTransport extends Avro
{
    @Override @SuppressWarnings("unchecked")
    public <T> T in(InputStream in, Class<T> type) throws IOException
    {
        byte[] bytes = ByteStreams.toByteArray(in);
        return (T) ByteBuffers.fromArray(bytes);
    }

    @Override @SuppressWarnings("unchecked")
    public <T> T in(InputStream in, TypeLiteral<T> type) throws IOException
    {
        byte[] bytes = ByteStreams.toByteArray(in);
        return (T) ByteBuffers.fromArray(bytes);
    }

    @Override
    public <T> void out(OutputStream out, Class<T> type, T data) throws IOException
    {
        ByteBuffer buffer = ByteBuffer.class.cast(data);
        out.write(ByteBuffers.toArray(buffer));
    }
}
