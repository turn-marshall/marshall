/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.transport;

import com.google.common.net.MediaType;
import com.google.inject.ImplementedBy;
import com.google.sitebricks.client.Transport;

/**
 * @author Chunzhao Zheng
 */
@ImplementedBy(AvroTransport.class)
public abstract class Avro implements Transport
{
    @Override
    public String contentType()
    {
        return MediaType.OCTET_STREAM.toString();
    }
}
