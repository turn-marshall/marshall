/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.transport;

import com.google.common.net.MediaType;
import com.google.inject.ImplementedBy;
import com.google.sitebricks.client.Transport;

/**
 * @author Chunzhao Zheng
 */
@ImplementedBy(JsonTransport.class)
public abstract class Json implements Transport
{
    public String contentType()
    {
        return MediaType.JSON_UTF_8.toString();
    }
}
