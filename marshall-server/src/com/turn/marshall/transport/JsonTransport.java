/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;

import com.turn.marshall.guice.Marshall;

/**
 * @author Chunzhao Zheng
 */
@Singleton
public class JsonTransport extends Json
{
    @Inject
    public JsonTransport(@Marshall Gson gson)
    {
        gson_ = gson;
    }

    @Override
    public <T> T in(InputStream in, Class<T> type) throws IOException
    {
        return gson_.fromJson(new InputStreamReader(in), type);
    }

    @Override
    public <T> T in(InputStream in, TypeLiteral<T> type) throws IOException
    {
        return gson_.fromJson(new InputStreamReader(in), type.getType());
    }

    @Override
    public <T> void out(OutputStream out, Class<T> type, T record) throws IOException
    {
        gson_.toJson(record, type, new OutputStreamWriter(out));
    }

    private final Gson gson_;
}
