/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.endpoint;

import java.nio.ByteBuffer;

import com.amazon.carbonado.RepositoryException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Post;

import org.joda.time.DateTime;

import com.turn.marshall.model.PropertyEntity;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.avro.ByteBufferAdapter;
import com.turn.marshall.shared.avro.generated.PropertyDeleteRequest;
import com.turn.marshall.storage.PropertyStorageService;
import com.turn.marshall.transport.Avro;

/**
 * @author Chunzhao Zheng
 */
@Service
@Singleton
public class DeletePropertyEndpoint
{
    @Inject
    public DeletePropertyEndpoint(PropertyStorageService service, ByteBufferAdapter<PropertyDeleteRequest> adapter)
    {
        service_ = service;
        adapter_ = adapter;
    }

    @Post
    public Reply<?> access(Request<String> request)
    {
        ByteBuffer buffer = request.read(ByteBuffer.class).as(Avro.class);
        PropertyDeleteRequest message = adapter_.fromByteBuffer(buffer);

        PropertyEntity entity = service_.prepare();
        entity.setPropertyKey(message.getPropertyName().toString());
        entity.setEntityStatus(EntityStatus.DELETED);

        String committer = message.getCommittedBy().toString();
        String description = message.getCommittedReason().toString();
        try {
            service_.delete(entity, committer, DateTime.now(), description);
        } catch (RepositoryException e) {
            return Reply.saying().error();
        }
        return Reply.saying().ok();
    }

    private final PropertyStorageService service_;
    private final ByteBufferAdapter<PropertyDeleteRequest> adapter_;
}
