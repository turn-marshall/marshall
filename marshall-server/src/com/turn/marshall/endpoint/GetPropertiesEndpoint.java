/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.endpoint;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

import com.amazon.carbonado.RepositoryException;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Post;

import com.turn.marshall.shared.PropertyKey;
import com.turn.marshall.shared.PropertyScope;
import com.turn.marshall.shared.avro.ByteBufferAdapter;
import com.turn.marshall.shared.avro.generated.PropertyMessage;
import com.turn.marshall.shared.avro.generated.PropertyRecord;
import com.turn.marshall.storage.PropertyStorageCache;
import com.turn.marshall.transport.Avro;
import com.turn.marshall.transport.Json;

/**
 * @author Chunzhao Zheng
 */
@Service
@Singleton
public class GetPropertiesEndpoint
{
    @Inject
    public GetPropertiesEndpoint(PropertyStorageCache cache, ByteBufferAdapter<PropertyMessage> adapter)
    {
        cache_ = cache;
        adapter_ = adapter;
    }

    @Post
    public Reply<ByteBuffer> access(Request<String> request) throws RepositoryException
    {
        PropertyScope scope = scope(request);

        // find all associated properties from property storage (secondary) cache
        Map<PropertyKey, String> properties = cache_.getProperties(scope);

        // construct avro message to reply
        List<PropertyRecord> records = Lists.newArrayList();
        PropertyMessage message = new PropertyMessage(records);
        for (Map.Entry<PropertyKey, String> property : properties.entrySet()) {
            PropertyKey key = property.getKey();
            String value = property.getValue();
            PropertyRecord record = new PropertyRecord(key.name(), key.group(), value);
            records.add(record);
        }

        // encode avro message to bytes
        ByteBuffer buffer = adapter_.toByteBuffer(message);
        return Reply.with(buffer).as(Avro.class);
    }

    protected PropertyScope scope(Request<String> request)
    {
        return request.read(PropertyScope.class).as(Json.class);
    }

    private final PropertyStorageCache cache_;
    private final ByteBufferAdapter<PropertyMessage> adapter_;
}
