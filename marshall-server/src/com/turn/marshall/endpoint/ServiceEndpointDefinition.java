/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.endpoint;

import com.turn.marshall.controller.ControllerAuditEndpoint;
import com.turn.marshall.controller.ControllerAuditsEndpoint;
import com.turn.marshall.controller.ControllerGroupsEndpoint;
import com.turn.marshall.controller.ControllerPropertiesEndpoint;
import com.turn.marshall.controller.ControllerPropertyEndpoint;
import com.turn.marshall.controller.ControllerScopesEndpoint;
import com.turn.marshall.shared.ServiceEndpoint;

/**
 * @author Chunzhao Zheng
 */
public enum ServiceEndpointDefinition
{
    PROPERTY_GET(ServiceEndpoint.PROPERTY_GET, GetPropertyEndpoint.class),
    PROPERTY_GET_ALL(ServiceEndpoint.PROPERTY_GET_ALL, GetPropertiesEndpoint.class),
    PROPERTY_CREATE(ServiceEndpoint.PROPERTY_CREATE, CreatePropertyEndpoint.class),
    PROPERTY_UPDATE(ServiceEndpoint.PROPERTY_UPDATE, CreatePropertyEndpoint.class),
    PROPERTY_DELETE(ServiceEndpoint.PROPERTY_DELETE, DeletePropertyEndpoint.class),
    CONTROLLER_GROUP_GET_ALL(ServiceEndpoint.CONTROLLER_GROUP_GET_ALL, ControllerGroupsEndpoint.class),
    CONTROLLER_SCOPE_GET_ALL(ServiceEndpoint.CONTROLLER_SCOPE_GET_ALL, ControllerScopesEndpoint.class),
    CONTROLLER_PROPERTY(ServiceEndpoint.CONTROLLER_PROPERTY, ControllerPropertyEndpoint.class),
    CONTROLLER_PROPERTIES(ServiceEndpoint.CONTROLLER_PROPERTY_GET_ALL, ControllerPropertiesEndpoint.class),
    CONTROLLER_AUDIT(ServiceEndpoint.CONTROLLER_AUDIT, ControllerAuditEndpoint.class),
    CONTROLLER_AUDITS(ServiceEndpoint.CONTROLLER_AUDITS, ControllerAuditsEndpoint.class),
    ;

    ServiceEndpointDefinition(ServiceEndpoint endpoint, Class<?> service)
    {
        endpoint_ = endpoint;
        service_ = service;
    }

    public ServiceEndpoint endpoint()
    {
        return endpoint_;
    }

    public String path()
    {
        return endpoint_.path();
    }

    public Class<?> service()
    {
        return service_;
    }

    private final ServiceEndpoint endpoint_;
    private final Class<?> service_;
}
