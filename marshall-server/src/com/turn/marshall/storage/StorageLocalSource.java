/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.storage;

import java.io.File;

import com.turn.marshall.shared.AppStage;

/**
 * @author Chunzhao Zheng
 */
public enum StorageLocalSource
{
    DEVELOPMENT(AppStage.DEVELOPMENT),
    SANDBOX(AppStage.SANDBOX),
    PRODUCTION(AppStage.PRODUCTION),
    ;

    StorageLocalSource(AppStage stage)
    {
        alias_ = name().toLowerCase();
        stage_ = stage;
    }

    public String alias()
    {
        return alias_;
    }

    public AppStage stage()
    {
        return stage_;
    }

    public String value()
    {
        return CURRENT_DIRECTORY + File.separator + "bdb" + File.separator + alias();
    }

    public static StorageLocalSource from(AppStage stage)
    {
        for (StorageLocalSource source : StorageLocalSource.values()) {
            if (source.stage() == stage) {
                return source;
            }
        }
        return DEVELOPMENT;
    }

    private final String alias_;
    private final AppStage stage_;
    private final static String CURRENT_DIRECTORY = System.getProperty("user.dir");
}
