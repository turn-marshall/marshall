package com.turn.marshall.storage;

import java.util.List;

import com.amazon.carbonado.Repository;
import com.amazon.carbonado.RepositoryException;
import com.amazon.carbonado.Transaction;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import com.turn.marshall.guice.Marshall;
import com.turn.marshall.model.PropertyAudit;
import com.turn.marshall.model.PropertyEntity;

@Singleton
public class AuditStorageService extends AuditableStorageService<PropertyEntity, PropertyAudit>{

	@Inject
	public AuditStorageService(@Marshall Repository repository) throws RepositoryException
	{
		super(repository, PropertyEntity.class, PropertyAudit.class);
	}
	
	public List<PropertyAudit> findAudits(final Long entityId) throws RepositoryException
    {
    	return execute(new TransactionCallback<List<PropertyAudit>>()
        {

            @Override
            public List<PropertyAudit> doInTransaction(Transaction txn)
                    throws RepositoryException
            {
                return null;
            }

        });
	}
}
