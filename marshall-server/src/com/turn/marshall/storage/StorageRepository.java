/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.storage;

import com.amazon.carbonado.Repository;
import com.amazon.carbonado.RepositoryException;
import com.amazon.carbonado.repo.jdbc.JDBCRepositoryBuilder;
import com.amazon.carbonado.repo.map.MapRepositoryBuilder;
import com.amazon.carbonado.repo.sleepycat.BDBProduct;
import com.amazon.carbonado.repo.sleepycat.BDBRepositoryBuilder;

import com.turn.marshall.shared.AppStage;

/**
 * @author Chunzhao Zheng
 */
public enum StorageRepository
{
    MAP
    {
        @Override
        public Repository repository(AppStage stage, String name) throws RepositoryException
        {
            MapRepositoryBuilder builder = new MapRepositoryBuilder();
            builder.setName(name);
            return builder.build();
        }
    },
    BDB
    {
        @Override
        public Repository repository(AppStage stage, String name) throws RepositoryException
        {
            StorageLocalSource source = StorageLocalSource.from(stage);
            BDBRepositoryBuilder builder = new BDBRepositoryBuilder();
            builder.setBDBProduct(BDBProduct.JE);
            builder.setTransactionNoSync(false);
            builder.setEnvironmentHome(source.value());
            builder.setName(name);
            return builder.build();
        }
    },
    JDBC
    {
        @Override
        public Repository repository(AppStage stage, String name) throws RepositoryException
        {
            StorageRemoteSource source = StorageRemoteSource.from(stage);
            JDBCRepositoryBuilder builder = new JDBCRepositoryBuilder();
            builder.setDataSource(source.value());
            builder.setName(name);
            builder.setAutoVersioningEnabled(true, null);
            return builder.build();
        }
    },
    ;

    public abstract Repository repository(AppStage stage, String name) throws RepositoryException;
}
