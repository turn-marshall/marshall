package com.turn.marshall.storage;

import java.util.Map;
import java.util.Map.Entry;

import com.amazon.carbonado.RepositoryException;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import com.turn.marshall.shared.PropertyKey;
import com.turn.marshall.shared.PropertyScope;
import com.turn.marshall.shared.ScopeType;

/**
 * @author Daniel Liu
 */
@Singleton
public class PropertyStorageCache
{
	private final PropertyStorageService service_;

    @Inject
    public PropertyStorageCache(PropertyStorageService service)
    {
    	service_ = service;
    }

    public String getProperty(PropertyScope scope, PropertyKey key) throws RepositoryException
    {
    	String result = null;
        Map<ScopeType, String> properties = service_.findPropertyEntities(scope, key);
        if (properties != null) {
            int highestPriority = -1;

            // return the value for the highest priority scope
            for (Map.Entry<ScopeType, String> entry : properties.entrySet()) {
                ScopeType scopeType = entry.getKey();

                if (scopeType.priority() > highestPriority) {
                    highestPriority = scopeType.priority();
                    result = entry.getValue();
                }
            }
        }
    	return result;
    }

    public Map<PropertyKey, String> getProperties(PropertyScope scope) throws RepositoryException
    {
        Map<PropertyKey, String> properties = Maps.newHashMap();
        Map<ScopeType, Map<PropertyKey, String>> map = service_.findPropertyEntities(scope);
        for (ScopeType scopeType : ScopeType.valuesInAscendingPriorityOrder()) {
            Map<PropertyKey, String> propertyKeyToValueMap = map.get(scopeType);
            if (propertyKeyToValueMap != null) {
                for (Entry<PropertyKey, String> entry : propertyKeyToValueMap.entrySet()) {
                    properties.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return properties;
    }
}
