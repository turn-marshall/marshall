package com.turn.marshall.storage;

import java.util.List;
import java.util.Map;

import com.amazon.carbonado.Query;
import com.amazon.carbonado.Repository;
import com.amazon.carbonado.RepositoryException;
import com.amazon.carbonado.Transaction;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import com.turn.marshall.guice.Marshall;
import com.turn.marshall.model.PropertyAudit;
import com.turn.marshall.model.PropertyEntity;
import com.turn.marshall.model.PropertyGroupEntity;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.model.PropertyScopeEntity;
import com.turn.marshall.shared.PropertyKey;
import com.turn.marshall.shared.PropertyScope;
import com.turn.marshall.shared.ScopeType;

/**
 * @author Ben Wang
 */
@Singleton
public class PropertyStorageService extends AuditableStorageService<PropertyEntity, PropertyAudit>
{
    @Inject
    public PropertyStorageService(@Marshall Repository repository) throws RepositoryException
    {
        super(repository, PropertyEntity.class, PropertyAudit.class);
    }

    public Map<ScopeType, String> findPropertyEntities(final PropertyScope scope, final PropertyKey key) throws RepositoryException
    {
        return execute(new TransactionCallback<Map<ScopeType, String>>()
        {
            @Override
            public Map<ScopeType, String> doInTransaction(Transaction txn) throws RepositoryException
            {
                Map<ScopeType, String> result = Maps.newHashMap();
                
                StringBuilder statement = new StringBuilder();
                
                // append query statement for property scope
                StringBuilder statementForScope = new StringBuilder();
                statementForScope.append("(").append("propertyScopeId = ?");
                for (int i = 0; i < scope.size(); i++) {
                	statementForScope.append(" | ").append("(").append("propertyScope.propertyScopeType = ? & propertyScope.propertyScope = ?").append(")");
                }
                statementForScope.append(")");
                
                // append query statement for property key
                StringBuilder statementForKey = new StringBuilder();
                statementForKey.append("propertyKey = ?").append(" & ");
                if (key.group() == null) {
                	statementForKey.append("propertyGroupId = ?");
                } else {
                	statementForKey.append("propertyGroup.propertyGroup = ?");
                }

                statement.append(statementForScope).append(" & ").append(statementForKey);
                
                // fill in query parameters for property scope;
                Query<PropertyEntity> query = storage().query(statement.toString()).with(null);
                for (final ScopeType type : scope.getScopeTypes()) {
                	 String value = scope.getScopeValue(type);
                	 query = query.with(type).with(value);
                }
                
                // fill in query parameters for property key
                query = query.with(key.name()).with(key.group());
                List<PropertyEntity> entities = load(query);
                
                for (PropertyEntity entity : entities) {
                	PropertyScopeEntity scopeEntity = entity.getPropertyScope();
                	if (scopeEntity == null) {
                		result.put(ScopeType.DEFAULT, entity.getPropertyValue());
                	} else {
                		result.put(scopeEntity.getPropertyScopeType(), entity.getPropertyValue());
                	}
                }
                
                return result;
            }
        });
    }

    public Map<ScopeType, Map<PropertyKey, String>> findPropertyEntities(final PropertyScope scope) throws RepositoryException
    {
        return execute(new TransactionCallback<Map<ScopeType, Map<PropertyKey, String>>>()
        {
            @Override
            public Map<ScopeType, Map<PropertyKey, String>> doInTransaction(Transaction txn) throws RepositoryException
            {
                Map<ScopeType, Map<PropertyKey, String>> result = Maps.newHashMap();
                Map<PropertyKey, String> map = Maps.newHashMap();
                result.put(ScopeType.DEFAULT, map);
                
                StringBuilder statement = new StringBuilder();
                statement.append("propertyScopeId = ?");

                for (int i = 0; i < scope.size(); i++) {
                    statement.append(" | ").append("(").append("propertyScope.propertyScopeType = ? & propertyScope.propertyScope = ?").append(")");
                }
                
                Query<PropertyEntity> query = storage().query(statement.toString()).with(null);
                for (final ScopeType type : scope.getScopeTypes()) {
                	Map<PropertyKey, String> map1 = Maps.newHashMap();
                	result.put(type, map1);
                	String value = scope.getScopeValue(type);
                	query = query.with(type).with(value);
                }
             
                List<PropertyEntity> entities = load(query);
                for (PropertyEntity entity : entities) {
                	PropertyGroupEntity group = entity.getPropertyGroup();
                	
                	PropertyScopeEntity propertyScope = entity.getPropertyScope();
                	ScopeType scopeType;
                	if (propertyScope == null) {
                		scopeType = ScopeType.DEFAULT;
                	} else {
                		scopeType = propertyScope.getPropertyScopeType();
                	}
                	
                	if (group == null) {
                		result.get(scopeType).put(PropertyKey.create(entity.getPropertyKey(), null), entity.getPropertyValue());
                		
                	} else {
                		result.get(scopeType).put(PropertyKey.create(entity.getPropertyKey(), group.getPropertyGroup()), entity.getPropertyValue());
                	}
                }
                return result;
            }
        });
    }
    
    public List<PropertyEntity> findProperties(final Long groupId, final Long scopeId) throws RepositoryException
    {
        return execute(new TransactionCallback<List<PropertyEntity>>()
        {
			@Override
			public List<PropertyEntity> doInTransaction(Transaction txn) throws RepositoryException
            {
			    String q = "";
			    if (groupId != null) {
			        q += "propertyGroupId = ? & "; 
			    }
			    
			    if (scopeId != null) {
			        q += "propertyScopeId = ? & ";
			    }
			    q += "entityStatus =?";
				Query<PropertyEntity> query = storage().query(q);
				if (groupId != null) {
				    query = query.with(groupId);
				}
				if (scopeId != null) {
				    query = query.with(scopeId);
				}
				query = query.with(EntityStatus.ACTIVED);
				return load(query);
			}
    	});
    }

    public List<PropertyAudit> findPropertyAudits(final long propertyId) throws RepositoryException
    {
        return execute(new TransactionCallback<List<PropertyAudit>>()
        {
            @Override
            public List<PropertyAudit> doInTransaction(Transaction txn) throws RepositoryException
            {
                PropertyEntity entity = prepareInternal();
                entity.setPropertyId(propertyId);
                entity = loadInternal(entity);

                Query<PropertyAudit> query = entity.getPropertyAudits();
                return loadAudits(query);
            }
        });
    }
}
