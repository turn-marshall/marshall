package com.turn.marshall.storage;

import com.amazon.carbonado.Repository;
import com.amazon.carbonado.RepositoryException;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import com.turn.marshall.guice.Marshall;
import com.turn.marshall.model.PropertyGroupAudit;
import com.turn.marshall.model.PropertyGroupEntity;

/**
 * @author Chunzhao Zheng
 */
@Singleton
public class PropertyGroupStorageService extends AuditableStorageService<PropertyGroupEntity, PropertyGroupAudit>
{
    @Inject
    public PropertyGroupStorageService(@Marshall Repository repository) throws RepositoryException
    {
        super(repository, PropertyGroupEntity.class, PropertyGroupAudit.class);
    }
    
}
