package com.turn.marshall.storage;

import java.util.List;

import com.amazon.carbonado.Query;
import com.amazon.carbonado.Repository;
import com.amazon.carbonado.RepositoryException;
import com.amazon.carbonado.Storable;
import com.amazon.carbonado.Storage;
import com.amazon.carbonado.Transaction;
import com.amazon.carbonado.gen.DetachedStorableFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Base storage service for Carbonado storables. Provides operations to
 * load an individual storable or an ordered slice of storable (which can
 * be used to retrieve all storables, if desired). All returned storables
 * are "externalized" to prevent mutation operations from bypassing the
 * service layer (which could result in changes being made without audit
 * entries getting created).
 *
 * @param <S> concrete {@link Storable} class type
 *
 * @author Chunzhao Zheng
 */
public abstract class AbstractStorageService<S extends Storable<S>>
{
    public AbstractStorageService(Repository repository, Class<S> storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "The storable class type cannot be null");

        repository_ = repository;
        storage_ = repository.storageFor(storable);
        factory_ = new DetachedStorableFactory<S>(storable);
    }

    /**
     * The storable returned by this method cannot be inserted or updated;
     * it must be passed to the appropriate service method to perform those
     * operations.
     *
     * @return An empty, newly allocated S instance.
     */
    public S prepare()
    {
        return factory_.newInstance();
    }

    /**
     * @return An empty, newly allocated S instance from the real repository.
     */
    protected S prepareInternal()
    {
        return storage_.prepare();
    }

    /**
     * Copies an internal storable to an external one (i.e., that doesn't
     * permit mutations or accessing of join properties).
     *
     * Returns null if the input is null
     *
     * @param internal The internal storable
     * @return An external storable
     */
    public S externalize(S internal)
    {
        S external = null;

        if (internal != null) {
            external = prepare();
            internal.copyAllProperties(external);
            external.markPropertiesClean();
        }

        return external;
    }

    /**
     * Copies an external storable to an internal one (i.e., that doesn't
     * permit mutations or accessing of join properties).
     *
     * @param external The external storable
     * @return An internal storable
     */
    public S internalize(S external)
    {
        Preconditions.checkArgument(external != null);

        S internal = prepareInternal();
        external.copyAllProperties(internal);
        internal.markPropertiesClean();
        return internal;
    }

    /**
     * Execute the provided callback within the scope of a transaction. The
     * transaction will be committed if the callback completes normally (i.e.,
     * doesn't throw an exception).
     *
     * @param callback The callback to execute
     * @return The value returned from callback
     * @throws com.amazon.carbonado.RepositoryException
     */
    public <T> T execute(TransactionCallback<T> callback) throws RepositoryException
    {
        T result = null;

        Transaction txn = repository_.enterTransaction();
        try {
            txn.setForUpdate(true);
            result = callback.doInTransaction(txn);
            txn.commit();
        } finally {
            txn.exit();
        }

        return result;
    }

    /**
     * Checks if {@link S} has a persistent record in the repository.
     * Either all primary key properties must be set, or all of the properties for one of its alternate keys.
     *
     * @param storable The storable to load.
     * @return True if the given storable has been persisted before.
     * @throws com.amazon.carbonado.RepositoryException
     */
    public boolean exists(final S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        return execute(new TransactionCallback<Boolean>()
        {
            @Override
            public Boolean doInTransaction(Transaction txn) throws RepositoryException
            {
                S internal = loadInternal(storable);
                return internal != null;
            }
        });
    }

    /**
     * Inserts the given instances of {@link S} into the repository.
     *
     * @param storables The {@link S} instances to insert
     * @return The inserted storables (including primary key)
     * @throws com.amazon.carbonado.RepositoryException
     */
    public Iterable<S> insert(final Iterable<S> storables) throws RepositoryException
    {
        Preconditions.checkArgument(storables != null, "storables must not be null");

        return execute(new TransactionCallback<Iterable<S>>()
        {
            @Override
            public Iterable<S> doInTransaction(Transaction txn) throws RepositoryException
            {
                ImmutableList.Builder<S> builder = new ImmutableList.Builder<S>();

                for (S storable : storables) {
                    S internal = insertInternal(storable);
                    S external = externalize(internal);
                    builder.add(external);
                }

                return builder.build();
            }
        });
    }

    /**
     * Inserts the given {@link S} into the repository.
     *
     * @param storable An instance of {@link S} to insert
     * @return A inserted storable (including primary key)
     * @throws com.amazon.carbonado.RepositoryException
     */
    public S insert(final S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        return execute(new TransactionCallback<S>()
        {
            @Override
            public S doInTransaction(Transaction txn) throws RepositoryException
            {
                S internal = insert(storable);
                return externalize(internal);
            }
        });
    }

    /**
     * Inserts the given {@link S} into the repository.
     *
     * @param storable An instance of {@link S} to insert
     * @return The inserted internal storables (including primary key)
     * @throws com.amazon.carbonado.RepositoryException
     */
    protected S insertInternal(S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        S internal = internalize(storable);
        internal.insert();
        return internal;
    }

    /**
     * Updates the given instances of {@link S} from the repository.
     *
     * @param storables The {@link S} instances to update
     * @return The updated storables
     * @throws com.amazon.carbonado.RepositoryException
     */
    public Iterable<S> update(final Iterable<S> storables) throws RepositoryException
    {
        Preconditions.checkArgument(storables != null, "storables must not be null");

        return execute(new TransactionCallback<Iterable<S>>()
        {
            @Override
            public Iterable<S> doInTransaction(Transaction txn) throws RepositoryException
            {
                ImmutableList.Builder<S> builder = new ImmutableList.Builder<S>();

                for (S storable : storables) {
                    S internal = updateInternal(storable);
                    S external = externalize(internal);
                    builder.add(external);
                }

                return builder.build();
            }
        });
    }

    /**
     * Updates the given {@link S} from the repository.
     *
     * @param storable An instance of {@link S} to update
     * @return A updated storable
     * @throws com.amazon.carbonado.RepositoryException
     */
    public S update(final S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storables must not be null");

        return execute(new TransactionCallback<S>()
        {
            @Override
            public S doInTransaction(Transaction txn) throws RepositoryException
            {
                S internal = updateInternal(storable);
                return externalize(internal);
            }
        });
    }

    /**
     * Updates the given {@link S} from the repository.
     *
     * @param storable An of {@link S} to update
     * @return The updated internal storable
     * @throws com.amazon.carbonado.RepositoryException
     */
    protected S updateInternal(S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        S internal = internalize(storable);
        internal.update();
        return internal;
    }


    /**
     * Deletes the given instances of {@link S} from the repository.
     *
     * @param storables The {@link S} instances to delete
     * @return The deleted storables
     * @throws com.amazon.carbonado.RepositoryException
     */
    public Iterable<S> delete(final Iterable<S> storables) throws RepositoryException
    {
        Preconditions.checkArgument(storables != null, "storables must not be null");

        return execute(new TransactionCallback<Iterable<S>>()
        {
            @Override
            public Iterable<S> doInTransaction(Transaction txn) throws RepositoryException
            {
                ImmutableList.Builder<S> builder = new ImmutableList.Builder<S>();

                for (S storable : storables) {
                    S internal = deleteInternal(storable);
                    S external = externalize(internal);
                    builder.add(external);
                }

                return builder.build();
            }
        });
    }

    /**
     * Deletes the given {@link S} from the repository.
     *
     * @param storable An instance of {@link S} to delete
     * @return The deleted storables
     * @throws com.amazon.carbonado.RepositoryException
     */
    public S delete(final S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        return execute(new TransactionCallback<S>()
        {
            @Override
            public S doInTransaction(Transaction txn) throws RepositoryException
            {
                S internal = deleteInternal(storable);
                return externalize(internal);
            }
        });
    }

    /**
     * Deletes the given {@link S} from the repository.
     *
     * @param storable An instance of {@link S} to delete
     * @return The updated internal storable (including updated version)
     * @throws com.amazon.carbonado.RepositoryException
     */
    protected S deleteInternal(S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        S internal = internalize(storable);
        internal.delete();

        return internal;
    }

    /**
     * Inserts or updates several {@link S} instances from the repository within
     * the same transaction, depending on if an instance of {@link S} has been persisted before.
     *
     * @param storables The {@link S} instances to insert or update
     * @return The inserted or updated storables (including primary key)
     * @throws com.amazon.carbonado.RepositoryException
     */
    public Iterable<S> save(final Iterable<S> storables) throws RepositoryException
    {
        Preconditions.checkArgument(storables != null, "storables must not be null");

        return execute(new TransactionCallback<Iterable<S>>()
        {
            @Override
            public Iterable<S> doInTransaction(Transaction txn) throws RepositoryException
            {
                ImmutableList.Builder<S> builder = new ImmutableList.Builder<S>();

                for (S storable : storables) {
                    S internal = saveInternal(storable);
                    S external = externalize(internal);
                    builder.add(external);
                }

                return builder.build();
            }
        });
    }

    /**
     * Inserts or updates the given {@link S} from the repository depending on
     * if the given {@link S} has been persisted before.
     *
     * @param storable An instance of {@link S} to insert or update
     * @return A inserted or updated storable (including primary key)
     * @throws com.amazon.carbonado.RepositoryException
     */
    public S save(final S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        return execute(new TransactionCallback<S>()
        {
            @Override
            public S doInTransaction(Transaction txn) throws RepositoryException
            {
                S internal = saveInternal(storable);
                return externalize(internal);
            }
        });
    }

    /**
     * Inserts or updates the given {@link S} from the repository depending on
     * if the given {@link S} has been persisted before.
     *
     * @param storable An instance of {@link S} to insert or update
     * @return The inserted or updated internal storable
     * @throws com.amazon.carbonado.RepositoryException
     */
    protected S saveInternal(S storable) throws RepositoryException
    {
        S internal = loadInternal(storable);
        if (internal == null) {
            internal = internalize(storable);
            internal.insert();
        } else {
            storable.copyUnequalProperties(internal);
            internal.update();
        }
        return internal;
    }

    /**
     * Refreshes (updates) several {@link S} instances from the repository within
     * the same transaction, or do nothing if {@link S} does not have associated
     * persistent record in the repository.
     *
     * @param storables The external {@link S} instances to refresh (update)
     * @return The refreshed storables (including primary key)
     * @throws com.amazon.carbonado.RepositoryException
     */
    public Iterable<S> refresh(final Iterable<S> storables) throws RepositoryException
    {
        Preconditions.checkArgument(storables != null, "storables must not be null");

        return execute(new TransactionCallback<Iterable<S>>()
        {
            @Override
            public Iterable<S> doInTransaction(Transaction txn) throws RepositoryException
            {
                ImmutableList.Builder<S> builder = new ImmutableList.Builder<S>();

                for (S storable : storables) {
                    S internal = refreshInternal(storable);
                    S external = externalize(internal);
                    if (external != null) {
                        builder.add(external);
                    }
                }

                return builder.build();
            }
        });
    }

    /**
     * Refresh the given {@link S} from the repository, or do nothing if this
     * {@link S} instance does not have associated persistent record in the repository.
     *
     * @param storable An instance of {@link S} to refresh (update)
     * @return A refreshed storable (including primary key)
     * @throws com.amazon.carbonado.RepositoryException
     */
    public S refresh(final S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        return execute(new TransactionCallback<S>()
        {
            @Override
            public S doInTransaction(Transaction txn) throws RepositoryException
            {
                S internal = refreshInternal(storable);
                return externalize(internal);
            }
        });
    }

    /**
     * Refresh the given {@link S} from the repository, or do nothing if this
     * {@link S} instance does not have associated persistent record in the repository.
     *
     * @param storable An instance of {@link S} to refresh (update)
     * @return The refreshed internal storable
     * @throws com.amazon.carbonado.RepositoryException
     */
    protected S refreshInternal(S storable) throws RepositoryException
    {
        S internal = loadInternal(storable);
        if (internal != null) {
            storable.copyUnequalProperties(internal);
            internal.update();
        }
        return internal;
    }

    /**
     * Loads a persistent instance of {@link S}, or null if the given {@link S} doesn't exist.
     * Note that this will be a different object than the passed-in storable.
     * Will return null if the storable cannot be loaded.
     * Either all primary key properties must be set, or all of the properties for one of its alternate keys.
     *
     * @param storable The {@link S} to load.
     * @return The loaded external storable.
     * @throws com.amazon.carbonado.RepositoryException
     */
    public S load(final S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        return execute(new TransactionCallback<S>()
        {
            @Override
            public S doInTransaction(Transaction txn) throws RepositoryException
            {
                S internal = loadInternal(storable);
                return externalize(internal);
            }
        });
    }

    /**
     * Loads a list of persistent {@link S} instances, in terms of the given {@link Query}.
     *
     * @param query The user-specified {@link Query} to load.
     * @return A list of loaded external storable instances.
     * @throws com.amazon.carbonado.RepositoryException
     */
    public List<S> load(final Query<S> query) throws RepositoryException
    {
        Preconditions.checkArgument(query != null, "query must not be null");

        return execute(new TransactionCallback<List<S>>()
        {
            @Override
            public List<S> doInTransaction(Transaction txn) throws RepositoryException
            {
                List<S> internals = query.fetch().toList();

                ImmutableList.Builder<S> builder = new ImmutableList.Builder<S>();

                for (S internal : internals) {
                    S external = externalize(internal);
                    builder.add(external);
                }

                return builder.build();
            }
        });
    }

    /**
     * Loads all instances of {@link S} from the repository.
     *
     * @return A list of loaded external storable instances.
     * @throws com.amazon.carbonado.RepositoryException
     */
    public List<S> loadAll() throws RepositoryException
    {
        return execute(new TransactionCallback<List<S>>()
        {
            @Override
            public List<S> doInTransaction(Transaction txn) throws RepositoryException
            {
                Query<S> query = storage_.query();
                List<S> internals = query.fetch().toList();

                ImmutableList.Builder<S> builder = new ImmutableList.Builder<S>();

                for (S internal : internals) {
                    S external = externalize(internal);
                    builder.add(external);
                }

                return builder.build();
            }
        });
    }

    /**
     * Loads the current version of a storable.
     *
     * Either all primary key properties must be set, or all of the properties for one of its alternate keys.
     *
     * @param storable An instance of {@link S} to load.
     * @return The loaded internal storable, or null if it couldn't be loaded
     * @throws com.amazon.carbonado.RepositoryException
     */
    protected S loadInternal(final S storable) throws RepositoryException
    {
        Preconditions.checkArgument(storable != null, "storable must not be null");

        S internal = internalize(storable);
        boolean loaded = internal.tryLoad();
        return loaded ? internal : null;
    }

    /**
     * Loads an ordered slice of {@link S}, starting at index 0 and
     * with 50 (default) maximum records to return.
     *
     * @param ordering The properties by which the slice should be ordered.
     *                 Prepend '-' to a property name for descending ordering.
     * @return The non-null result slice
     * @throws com.amazon.carbonado.RepositoryException
     */
    public QuerySlice<S> loadSlice(String... ordering) throws RepositoryException
    {
        return loadSlice(0, DEFAULT_MAX_RECORDS, ordering);
    }

    /**
     * Loads an ordered slice of {@link S}.
     *
     * @param ordering The properties by which the slice should be ordered.
     *                 Prepend '-' to a property name for descending ordering.
     * @param start 0-based start index for the results
     * @param limit Maximum number of records to return
     * @return The non-null result slice
     * @throws com.amazon.carbonado.RepositoryException
     */
    public QuerySlice<S> loadSlice(long start, int limit, String... ordering) throws RepositoryException
    {
        QuerySlice<S> slice = loadSliceInternal(storage_.query(), start, limit, ordering);
        ImmutableList.Builder<S> builder = new ImmutableList.Builder<S>();
        for (S internal : slice.getList()) {
            builder.add(externalize(internal));
        }

        return new QuerySlice<S>(builder.build(),
                slice.getOrdering(),
                slice.hasMore(),
                slice.getStartIndex());
    }

    /**
     * Loads an ordered slice of {@link S} from the given query instance.
     *
     * @param query The cached query instance to run with.
     * @param ordering The properties by which the slice should be ordered.
     *                 Prepend '-' to a property name for descending ordering.
     * @param start 0-based start index for the results
     * @param limit Maximum number of records to return
     * @return The non-null result slice
     * @throws com.amazon.carbonado.RepositoryException
     */
    protected QuerySlice<S> loadSliceInternal(final Query<S> query,
                                              final long start,
                                              final int limit,
                                              final String... ordering) throws RepositoryException
    {
        Preconditions.checkArgument(query != null, "query must not be null");
        Preconditions.checkArgument(start >= 0, "from must be >= 0");
        Preconditions.checkArgument(limit > 0, "limit must be > 0");
        Preconditions.checkArgument(ordering.length > 0, "ordering can't be empty");

        List<S> entities = execute(new TransactionCallback<List<S>>()
        {
            @Override
            public List<S> doInTransaction(Transaction txn) throws RepositoryException
            {
                long end = start + limit + 1; // +1 to detect if more results available
                return query.orderBy(ordering).fetchSlice(start, end).toList();
            }
        });

        entities = ImmutableList.copyOf(entities);
        boolean hasMore = entities.size() > limit;
        if (hasMore) {
            entities = entities.subList(0, limit);
        }

        return new QuerySlice<S>(entities, ordering, hasMore, start);
    }

    /**
     * @return The underlying {@link Repository} used by this storage service.
     */
    public Repository repository()
    {
        return repository_;
    }

    public Storage<S> storage()
    {
        return storage_;
    }

    private final DetachedStorableFactory<S> factory_;
    private final Storage<S> storage_;
    private final Repository repository_;

    private final static int DEFAULT_MAX_RECORDS = 50;
}
