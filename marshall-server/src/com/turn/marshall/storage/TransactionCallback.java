package com.turn.marshall.storage;

import com.amazon.carbonado.RepositoryException;
import com.amazon.carbonado.Transaction;

/**
 * Callback interface for running logic w/in the scope of a Carbonado transaction.
 *
 * @param <T> Type returned from doInTransaction
 *
 * @author Chunzhao Zheng
 */
public interface TransactionCallback<T> {

    /**
     * Logic to be executed within the scope of a Carbonado transaction
     *
     * @param txn The transaction. Implementations in general should not call commit() or exit().
     * @return Whatever is desired
     * @throws com.amazon.carbonado.RepositoryException
     */
    public T doInTransaction(Transaction txn) throws RepositoryException;
}
