package com.turn.marshall.storage;

import com.amazon.carbonado.Repository;
import com.amazon.carbonado.RepositoryException;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import com.turn.marshall.guice.Marshall;
import com.turn.marshall.model.PropertyScopeAudit;
import com.turn.marshall.model.PropertyScopeEntity;

/**
 * @author Chunzhao Zheng
 */
@Singleton
public class PropertyScopeStorageService extends AuditableStorageService<PropertyScopeEntity, PropertyScopeAudit>
{
    @Inject
    public PropertyScopeStorageService(@Marshall Repository repository) throws RepositoryException
    {
        super(repository, PropertyScopeEntity.class, PropertyScopeAudit.class);
    }
}
