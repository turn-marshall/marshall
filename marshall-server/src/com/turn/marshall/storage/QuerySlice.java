package com.turn.marshall.storage;

import java.util.List;

import com.google.common.base.Preconditions;

/**
 * An ordered, possibly partial, slice of results
 *
 * @param <E> Type of the entity
 *
 * @author Chunzhao Zheng
 */
public class QuerySlice<E>
{
    /**
     * Full constructor that includes extra details for possibly partial results
     *
     * @param entities Immutable list of entities, ordered by ordering
     * @param ordering Ordering of the results
     * @param hasMore True if more records are available (if a limit was specified
     *  when loading the result slice)
     * @param startIndex The start index of the results
     */
    public QuerySlice(List<E> entities, String[] ordering, boolean hasMore, long startIndex)
    {
        Preconditions.checkArgument(entities != null, "entities can't be null (empty is ok)");
        Preconditions.checkArgument(ordering != null, "ordering can't be null");
        Preconditions.checkArgument(startIndex >= 0, "startIndex must be >= 0");

        this.entities = entities;
        this.ordering_ = ordering;
        this.hasMore_ = hasMore;
        this.startIndex_ = startIndex;
    }

    /**
     * Constructor for full results
     *
     * @param results Immutable list of results, ordered by ordering
     * @param ordering Ordering of the results
     */
    public QuerySlice(List<E> results, String[] ordering)
    {
        this(results, ordering, false, 0);
    }

    /**
     * @return True if more records are available (if a limit was specified when loading the result slice)
     */
    public boolean hasMore()
    {
        return hasMore_;
    }

    /**
     * @return Immutable list of results, ordered by {@link #getOrdering()}
     */
    public List<E> getList()
    {
        return entities;
    }

    /**
     * @return Ordering of the results list
     */
    public String[] getOrdering()
    {
        return ordering_;
    }

    /**
     * @return The zero-based start index of the results (i.e., how many results
     *  were skipped)
     */
    public long getStartIndex()
    {
        return startIndex_;
    }

    /**
     * Note that this method will always return a result, even if getMoreRecordsAvailable()
     * return false.
     *
     * @return The start index to use to retrieve the next batch of results
     */
    public long getNextStartIndex()
    {
        return startIndex_ + entities.size();
    }

    private final List<E> entities;
    private final boolean hasMore_;
    private final String[] ordering_;
    private final long startIndex_;
}
