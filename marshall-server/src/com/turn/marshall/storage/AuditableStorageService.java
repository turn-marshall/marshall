package com.turn.marshall.storage;

import java.util.List;

import com.amazon.carbonado.Query;
import com.amazon.carbonado.Repository;
import com.amazon.carbonado.RepositoryException;
import com.amazon.carbonado.Storable;
import com.amazon.carbonado.Storage;
import com.amazon.carbonado.Transaction;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import com.turn.marshall.model.Auditable;
import com.turn.marshall.model.Entity;
import com.turn.marshall.shared.UserAction;

/**
 * @param <E> concrete {@link Entity} class type
 * @param <A> concrete {@link Auditable} class type
 *
 * @author Chunzhao Zheng
 */
public abstract class AuditableStorageService<E extends Entity<E>, A extends Storable<A> & Auditable<E, A>> extends AbstractStorageService<E>
{
    public AuditableStorageService(Repository repository, Class<E> entity, Class<A> auditable) throws RepositoryException
    {
        super(repository, entity);
        storage_ = repository.storageFor(auditable);
    }

    public E insert(E entity, String committer, DateTime time, String reason) throws RepositoryException
    {
        Preconditions.checkArgument(entity != null, "entity must not be null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(committer), "committer is required");

        E internal = internalize(entity);
        entity = insertInternal(internal, committer, time, reason);
        return externalize(entity);
    }

    protected E insertInternal(final E entity,
                               final String committer,
                               final DateTime time,
                               final String reason) throws RepositoryException
    {
        return execute(new TransactionCallback<E>()
        {
            @Override
            public E doInTransaction(Transaction txn) throws RepositoryException
            {
                entity.insert();

                audit(entity, committer, time, reason, UserAction.CREATE);

                return entity;
            }
        });
    }

    public E update(E entity, String committer, DateTime time, String reason) throws RepositoryException
    {
        Preconditions.checkArgument(entity != null, "entity must not be null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(committer), "committer is required");

        E internal = internalize(entity);
        entity = updateInternal(internal, committer, time, reason);
        return externalize(entity);
    }

    protected E updateInternal(final E entity,
                               final String committer,
                               final DateTime time,
                               final String reason) throws RepositoryException
    {
        return execute(new TransactionCallback<E>()
        {
            @Override
            public E doInTransaction(Transaction txn) throws RepositoryException
            {
                entity.update();

                audit(entity, committer, time, reason, UserAction.MODIFY);

                return entity;
            }
        });
    }

    public E delete(E entity, String committer, DateTime time, String reason) throws RepositoryException
    {
        Preconditions.checkArgument(entity != null, "entity must not be null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(committer), "committer is required");

        E internal = internalize(entity);
        entity = deleteInternal(internal, committer, time, reason);
        return externalize(entity);
    }

    protected E deleteInternal(final E entity,
                               final String committer,
                               final DateTime time,
                               final String reason) throws RepositoryException
    {
        Preconditions.checkArgument(entity != null, "entity must not be null");

        return execute(new TransactionCallback<E>()
        {
            @Override
            public E doInTransaction(Transaction txn) throws RepositoryException
            {
                entity.delete();
                audit(entity, committer, time, reason, UserAction.DELETE);
                return entity;
            }
        });
    }

    public List<A> loadAudits() throws RepositoryException
    {
        List<A> audits = loadAudits(storage_.query());
        ImmutableList.Builder<A> builder = new ImmutableList.Builder<A>();
        for (A audit : audits) {
            builder.add(audit);
        }
        return builder.build();
    }

    protected List<A> loadAudits(final Query<A> query) throws RepositoryException
    {
        return execute(new TransactionCallback<List<A>>()
        {
            @Override
            public List<A> doInTransaction(Transaction txn) throws RepositoryException
            {
                return query.fetch().toList();
            }
        });
    }

    protected A audit(E entity,
                      String committer,
                      DateTime time,
                      String reason,
                      UserAction action) throws RepositoryException
    {
        A auditable = storage_.prepare();
        auditable = auditable.fillFrom(entity);

        // set common entity information
        auditable.setEntityStatus(entity.getEntityStatus());

        // set common audit information
        auditable.setCommittedBy(committer);
        auditable.setCommittedDate(time);
        auditable.setCommittedReason(reason);
        auditable.setUserAction(action);

        auditable.insert();
        return auditable;
    }

    private final Storage<A> storage_; // audit storage
}
