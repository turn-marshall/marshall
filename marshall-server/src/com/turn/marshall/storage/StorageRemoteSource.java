/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.storage;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import com.turn.marshall.shared.AppStage;

/**
 * @author Chunzhao Zheng
 */
public enum StorageRemoteSource
{
    DEVELOPMENT(AppStage.DEVELOPMENT, "172.16.203.10", 5432, "marshall", "marshall", "marshall"),
    SANDBOX(AppStage.SANDBOX, "172.16.203.10", 5432, "marshall", "marshall", "marshall"),
    PRODUCTION(AppStage.PRODUCTION, "172.16.203.10", 5432, "marshall", "marshall", "marshall"),
    ;

    StorageRemoteSource(AppStage stage, String hostname, int port, String db, String username, String password)
    {
        stage_ = stage;

        url_ = PROTOCOL + hostname + ":" + port + "/" + db;

        value_ = new BasicDataSource();
        value_.setUrl(url_);
        value_.setUsername(username);
        value_.setPassword(password);
        value_.setDriverClassName(DRIVER);
    }

    public AppStage stage()
    {
        return stage_;
    }

    public DataSource value()
    {
        return value_;
    }

    public String url()
    {
        return url_;
    }

    public static StorageRemoteSource from(AppStage stage)
    {
        for (StorageRemoteSource source : StorageRemoteSource.values()) {
            if (source.stage() == stage) {
                return source;
            }
        }
        return DEVELOPMENT;
    }

    private final AppStage stage_;
    private final String url_;
    private final BasicDataSource value_;

    private final static String PROTOCOL = "jdbc:postgresql://";
    private final static String DRIVER = "org.postgresql.Driver";
}
