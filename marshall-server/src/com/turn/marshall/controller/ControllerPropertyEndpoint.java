package com.turn.marshall.controller;

import java.util.HashMap;
import java.util.Map;

import com.amazon.carbonado.RepositoryException;
import org.joda.time.DateTime;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.sitebricks.client.transport.Json;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Delete;
import com.google.sitebricks.http.Get;
import com.google.sitebricks.http.Post;
import com.google.sitebricks.http.Put;

import com.turn.marshall.model.PropertyEntity;
import com.turn.marshall.model.PropertyGroupEntity;
import com.turn.marshall.model.PropertyScopeEntity;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.json.PropertyRecord;
import com.turn.marshall.storage.PropertyGroupStorageService;
import com.turn.marshall.storage.PropertyScopeStorageService;
import com.turn.marshall.storage.PropertyStorageService;

@Service
@Singleton
public class ControllerPropertyEndpoint
{
    private static final String UPDATER_NAME = "Admin";

    @Inject
    public ControllerPropertyEndpoint(PropertyStorageService psService,
                                      PropertyGroupStorageService pgsService,
                                      PropertyScopeStorageService pssService)
    {
        psService_ = psService;
        pgsService_ = pgsService;
        pssService_ = pssService;
    }

    @Post
    public Reply<Map<String, String>> create(Request<String> request)
    {
        PropertyRecord pr = request.read(PropertyRecord.class).as(Json.class);
        Map<String, String> response = new HashMap<String, String>();
        if (pr.getKey() == null || pr.getValue() == null || pr.getReason() == null) {
            response.put("error", "keys, values and reasons cannot be null");
            return Reply.with(response).badRequest();
        }
        PropertyEntity pe = psService_.prepare();
        try {
            generatePropertyRecord(pr, pe, EntityStatus.ACTIVED);
            pe.setEntityStatus(EntityStatus.ACTIVED);
            psService_.insert(pe, UPDATER_NAME, DateTime.now(), pr.getReason());
        } catch (RepositoryException e) {
            response.put("error", e.getMessage());
            return Reply.with(response).badRequest();
        }
        response.put("success", "insertion sucessful");
        return Reply.with(response).as(Json.class);
    }

    @Get
    public Reply<?> read(Request<String> request)
    {
        String entityInput = request.param("entityId");
        Long entityId = Long.getLong(entityInput);
        Map<String, String> response = new HashMap<String, String>();
        PropertyRecord record = PropertyRecord.create();
        PropertyEntity pe = psService_.prepare();
        pe.setPropertyId(entityId);
        try {
            pe = psService_.load(pe);
        } catch (RepositoryException e) {
            response.put("error", "Invalid error Entity Id");
            return Reply.with(response).badRequest();
        }
        if (pe == null) {
            response.put("error", "Invalid error Entity Id");
            return Reply.with(response).badRequest();
        } else {
            record.setEntityId(pe.getPropertyId());
            record.setEntityStatus(pe.getEntityStatus());
            record.setGroupId(pe.getPropertyGroupId());
            record.setKey(pe.getPropertyKey());
            record.setScopeId(pe.getPropertyScopeId());
            record.setValue(pe.getPropertyValue());
            return Reply.with(record).as(Json.class);
        }
    }

    @Put
    public Reply<Map<String, String>> update(Request<String> request)
    {
        PropertyRecord pr = request.read(PropertyRecord.class).as(Json.class);
        PropertyEntity pe = psService_.prepare();

        Map<String, String> response = new HashMap<String, String>();
        if (pr.getEntityId() == null || pr.getEntityId() < 0 || pr.getValue() == null || pr.getReason() == null) {
            response.put("error", "entityId, values and reasons cannot be null");
            return Reply.with(response).badRequest();
        }
        try {
            pe.setPropertyId(pr.getEntityId());
            pe = psService_.load(pe);
            generatePropertyRecord(pr, pe, pr.getEntityStatus());
            psService_.update(pe, UPDATER_NAME, DateTime.now(), pr.getReason());
        } catch (RepositoryException e) {
            response.put("error", e.getMessage());
            return Reply.with(response).badRequest();
        }
        return Reply.with(response).as(Json.class);
    }

    @Delete
    public Reply<Map<String, String>> delete(Request<String> request)
    {
        PropertyRecord pr = request.read(PropertyRecord.class).as(Json.class);
        PropertyEntity pe = psService_.prepare();
        Map<String, String> response = new HashMap<String, String>();
        if (pr.getEntityId() == null || pr.getEntityId() < 0 || pr.getReason() == null) {
            response.put("error", "entityId, values and reasons cannot be null");
            return Reply.with(response).badRequest();
        }
        try {
            pe.setPropertyId(pr.getEntityId());
            pe = psService_.load(pe);
//			generatePropertyRecord(pr, pe, pr.getEntityStatus());
            pe.setEntityStatus(EntityStatus.DELETED);
            psService_.update(pe, UPDATER_NAME, DateTime.now(), pr.getReason());
        } catch (RepositoryException e) {
            response.put("error", e.getMessage());
            return Reply.with(response).badRequest();
        }
        return Reply.with(response).as(Json.class);
    }

    private void generatePropertyRecord(PropertyRecord pr,
                                        PropertyEntity pe,
                                        EntityStatus es) throws RepositoryException
    {
        pe.setPropertyKey(pr.getKey());
        pe.setPropertyValue(pr.getValue());
        if (pr.getGroupId() != null && pr.getGroupId() > -1) {
            PropertyGroupEntity pge = pgsService_.prepare();
            pge.setPropertyGroupId(pr.getGroupId());
            pe.setPropertyGroup(pgsService_.load(pge));
        }
        PropertyScopeEntity pse = null;
        if (pr.getScopeId() != null && pr.getScopeId() > -1) {
            pse = pssService_.prepare();
            pse.setPropertyScopeId(pr.getScopeId());
            pe.setPropertyScope(pssService_.load(pse));
        }
    }

    private final PropertyStorageService psService_;
    private final PropertyScopeStorageService pssService_;
    private final PropertyGroupStorageService pgsService_;
}
