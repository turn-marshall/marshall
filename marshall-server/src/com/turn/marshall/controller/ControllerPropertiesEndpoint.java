package com.turn.marshall.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.sitebricks.client.transport.Json;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Get;
import com.google.sitebricks.http.Post;

import com.turn.marshall.model.PropertyEntity;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.json.PropertyRecord;
import com.turn.marshall.storage.PropertyStorageService;

@Service
@Singleton
public class ControllerPropertiesEndpoint
{
 	@Inject
    public ControllerPropertiesEndpoint(PropertyStorageService service)
    {
        service_ = service;
    }

 	@Get
    public Reply<List<PropertyRecord>> readAll(Request<String> request) throws RepositoryException
    {
        String strGroupId = request.param("groupId");
        Long groupId = Long.parseLong(strGroupId);
        groupId = (groupId == -1L ? null : groupId);

        String strScopeId = request.param("scopeId");
        Long scopeId = Long.parseLong(strScopeId);
        scopeId = (scopeId == -1L ? null : scopeId);
 		
 		List<PropertyRecord> prList = new ArrayList<PropertyRecord>();
 		
 		List<PropertyEntity> peList = service_.findProperties(groupId, scopeId);
 		for(PropertyEntity pe : peList) {
 			PropertyRecord record = PropertyRecord.create();
 			record.setEntityId(pe.getPropertyId());
 			record.setEntityStatus(pe.getEntityStatus());
 			record.setGroupId(pe.getPropertyGroupId());
 			record.setKey(pe.getPropertyKey());
 			record.setScopeId(pe.getPropertyScopeId());
 			record.setValue(pe.getPropertyValue());
 			prList.add(record);
 		}
 		
 		return Reply.with(prList).as(Json.class);
    }
 	
 	@Post
 	public Reply<Map<String, String>> insertAll(Request<String> request) throws RepositoryException
    {
 		PropertyEntity pe = service_.prepare();
 		pe.setEntityStatus(EntityStatus.ACTIVED);
 		pe.setPropertyGroupId(1L);
 		pe.setPropertyId(10000);
 		pe.setPropertyKey("mykey");
 		pe.setPropertyScopeId(2L);
 		pe.setPropertyValue("myValue");
 		service_.insert(pe);
 		Map<String, String> map = new HashMap<String, String>();
 		return Reply.with(map).as(Json.class);
 	}
 	
 	
 	private final PropertyStorageService service_;
 	
}
