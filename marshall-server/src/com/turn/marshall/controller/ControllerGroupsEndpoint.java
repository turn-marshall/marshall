package com.turn.marshall.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.sitebricks.client.transport.Json;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Get;
import com.google.sitebricks.http.Post;

import com.turn.marshall.model.PropertyGroupEntity;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.json.PropertyGroup;
import com.turn.marshall.storage.PropertyGroupStorageService;

@Service
@Singleton
public class ControllerGroupsEndpoint
{
    @Inject
    public ControllerGroupsEndpoint(PropertyGroupStorageService service)
    {
        service_ = service;
    }

    @Get
    public Reply<List<PropertyGroup>> read(Request<String> request) throws RepositoryException
    {
        List<PropertyGroupEntity> pgeList = service_.loadSlice("propertyGroupId").getList();
        List<PropertyGroup> pgList = new ArrayList<PropertyGroup>();
        for (PropertyGroupEntity pge : pgeList) {
            pgList.add(PropertyGroup.create(pge.getPropertyGroupId(), pge.getPropertyGroup()));
        }
        return Reply.with(pgList).as(Json.class);
    }

    @Post
    public Reply<Map<String, String>> create(Request<String> request) throws RepositoryException
    {
        for (int i = 0; i < 100; i++) {
            PropertyGroupEntity pge = service_.prepare();
            pge.setPropertyGroupId(i);
            pge.setPropertyGroup(i + "");
            pge.setEntityStatus(EntityStatus.ACTIVED);
            service_.insert(pge);
        }
        Map<String, String> res = new HashMap<String, String>();
        return Reply.with(res).as(Json.class);
    }

    private final PropertyGroupStorageService service_;
}
