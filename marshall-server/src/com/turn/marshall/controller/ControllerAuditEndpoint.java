package com.turn.marshall.controller;

import java.util.ArrayList;
import java.util.List;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.sitebricks.client.transport.Json;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Get;

import org.apache.commons.lang.math.NumberUtils;

import com.turn.marshall.shared.json.PropertyAudit;
import com.turn.marshall.storage.PropertyStorageService;

@Service
@Singleton
public class ControllerAuditEndpoint
{
	@Inject
	public ControllerAuditEndpoint(PropertyStorageService service)
    {
		service_ = service;
	}

	@Get
	public Reply<List<PropertyAudit>> read(Request<String> request) throws RepositoryException
    {
		// service_.findPropertyAudits(propertyId);
		String entityInput = request.param("entityId");
		long entityId = NumberUtils.toLong(entityInput);
		List<com.turn.marshall.shared.json.PropertyAudit> paJsonList = new ArrayList<com.turn.marshall.shared.json.PropertyAudit>();
        List<com.turn.marshall.model.PropertyAudit> propertyAuditList = service_.findPropertyAudits(entityId);
        for (com.turn.marshall.model.PropertyAudit pa : propertyAuditList) {
            paJsonList.add(com.turn.marshall.shared.json.PropertyAudit.create(pa.getPropertyAuditId(),
                    pa.getPropertyScopeId(), pa.getPropertyGroupId(), pa.getPropertyKey(), pa.getPropertyValue(),
                    pa.getEntityStatus(), pa.getEntityVersion(), pa.getCommittedBy(), pa.getCommittedReason(),
                    pa.getUserAction(), pa.getCommittedDate().toDate()));
        }
		return Reply.with(paJsonList).as(Json.class);
	}

	private final PropertyStorageService service_;
}
