package com.turn.marshall.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.sitebricks.client.transport.Json;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.headless.Service;
import com.google.sitebricks.http.Get;
import com.google.sitebricks.http.Post;

import com.turn.marshall.model.PropertyScopeEntity;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.ScopeType;
import com.turn.marshall.shared.json.PropertyScope;
import com.turn.marshall.storage.PropertyScopeStorageService;

@Service
@Singleton
public class ControllerScopesEndpoint
{
    @Inject
    public ControllerScopesEndpoint(PropertyScopeStorageService service)
    {
        service_ = service;
    }

    @Get
    public Reply<List<PropertyScope>> read(Request<String> request) throws RepositoryException
    {
        List<PropertyScopeEntity> pgeList = service_.loadSlice("propertyScopeId").getList();
        List<PropertyScope> pgList = new ArrayList<PropertyScope>();
        for (PropertyScopeEntity pge : pgeList) {
            pgList.add(PropertyScope.create(pge.getPropertyScopeId(), pge.getPropertyScope()));
        }
        return Reply.with(pgList).as(Json.class);
    }

    @Post
    public Reply<Map<String, String>> create(Request<String> request) throws RepositoryException
    {
        for (int i = 0; i < 100; i++) {
            PropertyScopeEntity pge = service_.prepare();
            pge.setPropertyScopeId(i);
            pge.setPropertyScope(i + "");
            pge.setEntityStatus(EntityStatus.ACTIVED);
            pge.setPropertyScopeType(ScopeType.DATA_CENTER);
            service_.insert(pge);
        }
        Map<String, String> res = new HashMap<String, String>();
        return Reply.with(res).as(Json.class);
    }

    private final PropertyScopeStorageService service_;
}
