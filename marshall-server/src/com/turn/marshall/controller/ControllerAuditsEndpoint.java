package com.turn.marshall.controller;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Inject;
import com.google.sitebricks.client.transport.Json;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.http.Get;

import com.turn.marshall.shared.json.PropertyAudit;
import com.turn.marshall.storage.PropertyStorageService;

public class ControllerAuditsEndpoint
{
 	@Inject
    public ControllerAuditsEndpoint(PropertyStorageService service)
    {
        service_ = service;
    }

 	@Get
    public Reply<PropertyAudit> read(Request<String> request) throws RepositoryException
    {
// 		List<PropertyGroupEntity> pgeList = service_.loadSlice("propertyGroupId").getList();

 		return Reply.with(PropertyAudit.create()).as(Json.class);
    }
 	private final PropertyStorageService service_;
}
