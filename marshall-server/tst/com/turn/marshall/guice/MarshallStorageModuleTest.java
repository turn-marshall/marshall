/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.guice;

import com.amazon.carbonado.Repository;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.turn.marshall.shared.AppStage;
import com.turn.marshall.storage.StorageRepository;

/**
 * @author Chunzhao Zheng
 */
@Test(groups = { "unittest" })
public class MarshallStorageModuleTest
{
    private static Injector injector;

    @BeforeClass
    public static void createStorageModule()
    {
        MarshallStorageModule module = new MarshallStorageModule(AppStage.DEVELOPMENT, StorageRepository.MAP);
        injector = Guice.createInjector(Stage.PRODUCTION, module);
        Assert.assertNotNull(injector);
    }

    @Test
    public void initializeStorageComponents()
    {
        Repository repository1 = injector.getInstance(Repository.class);
        Repository repository2 = injector.getInstance(Repository.class);
        Assert.assertNotNull(repository1);
        Assert.assertNotNull(repository2);
        Assert.assertSame(repository1, repository2);
    }
}
