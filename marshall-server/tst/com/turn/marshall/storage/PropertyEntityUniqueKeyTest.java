package com.turn.marshall.storage;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

import org.joda.time.DateTime;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.turn.marshall.guice.MarshallStorageModule;
import com.turn.marshall.model.PropertyEntity;
import com.turn.marshall.model.PropertyGroupEntity;
import com.turn.marshall.model.PropertyScopeEntity;
import com.turn.marshall.shared.AppStage;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.ScopeType;

/**
 * @author Chunzhao Zheng
 */
@Test(groups = { "unittest" })
public class PropertyEntityUniqueKeyTest
{
    private static PropertyStorageService propertyService;
    private static PropertyGroupStorageService propertyGroupService;
    private static PropertyScopeStorageService propertyScopeService;

    @BeforeClass
    public void initialize()
    {
        Injector injector = Guice.createInjector(Stage.PRODUCTION, new MarshallStorageModule(AppStage.DEVELOPMENT, StorageRepository.BDB));
        propertyService = injector.getInstance(PropertyStorageService.class);
        propertyGroupService = injector.getInstance(PropertyGroupStorageService.class);
        propertyScopeService = injector.getInstance(PropertyScopeStorageService.class);
    }

    @Test
    public void createPropertyEntitiesWithSamePropertyKey() throws RepositoryException
    {
        PropertyGroupEntity group = propertyGroupService.prepare();
        group.setPropertyGroup("group-1");
        group.setEntityStatus(EntityStatus.ACTIVED);
        group = propertyGroupService.insert(group, "tester", DateTime.now(), "test create");

        PropertyScopeEntity scope = propertyScopeService.prepare();
        scope.setPropertyScopeType(ScopeType.DATA_CENTER);
        scope.setPropertyScope("ds-1");
        scope.setEntityStatus(EntityStatus.ACTIVED);
        scope = propertyScopeService.insert(scope, "tester", DateTime.now(), "test create");

        PropertyEntity entity1 = propertyService.prepare();
        entity1.setPropertyKey("key-1");
        entity1.setPropertyValue("value-1");
        entity1.setPropertyScope(scope);
        entity1.setEntityStatus(EntityStatus.ACTIVED);
        entity1 = propertyService.insert(entity1, "tester", DateTime.now(), "test create");

        PropertyEntity entity2 = propertyService.prepare();
        entity2.setPropertyKey("key-1");
        entity2.setPropertyValue("value-2");
        entity2.setPropertyGroup(group);
        entity2.setEntityStatus(EntityStatus.ACTIVED);
        entity2 = propertyService.insert(entity2, "tester", DateTime.now(), "test create");

        propertyGroupService.delete(group);
        propertyScopeService.delete(scope);

        propertyService.delete(entity1);
        propertyService.delete(entity2);
    }

}
