/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.storage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.util.List;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

import org.joda.time.DateTime;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.turn.marshall.guice.MarshallStorageModule;
import com.turn.marshall.model.PropertyGroupEntity;
import com.turn.marshall.shared.AppStage;
import com.turn.marshall.shared.EntityStatus;

/**
 * @author Chunzhao Zheng
 */
@Test(groups = { "unittest" })
public class PropertyGroupStorageServiceTest
{
    private static PropertyGroupStorageService service;

    @BeforeClass
    public void initialize()
    {
        Injector injector = Guice.createInjector(Stage.PRODUCTION, new MarshallStorageModule(AppStage.DEVELOPMENT, StorageRepository.BDB));
        service = injector.getInstance(PropertyGroupStorageService.class);
    }

    @Test
    public void createPropertyGroupEntity() throws RepositoryException
    {
        PropertyGroupEntity entity = service.prepare();
        entity.setPropertyGroup("group-1");
        entity.setEntityStatus(EntityStatus.ACTIVED);

        entity = service.insert(entity, "tester", DateTime.now(), "test create");
        assertNotNull(entity);

        service.delete(entity);
    }

    @Test
    public void findPropertyEntity() throws RepositoryException
    {
        PropertyGroupEntity entity1 = service.prepare();
        entity1.setPropertyGroup("group-1");
        entity1.setEntityStatus(EntityStatus.ACTIVED);
        entity1 = service.insert(entity1, "tester", DateTime.now(), "test create");

        PropertyGroupEntity entity2 = service.prepare();
        entity2.setPropertyGroup("group-2");
        entity2.setEntityStatus(EntityStatus.ACTIVED);
        entity2 = service.insert(entity2, "tester", DateTime.now(), "test create");

        List<PropertyGroupEntity> entities = service.loadAll();
        assertNotNull(entities);
        assertEquals(entities.size(), 2);
        for (PropertyGroupEntity entity : entities) {
        	assertEquals(entity.getEntityStatus(), EntityStatus.ACTIVED);
        }

        service.delete(entity1);
        service.delete(entity2);
    }
}
