/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.storage;

import static org.testng.Assert.*;

import java.util.Map;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

import org.joda.time.DateTime;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.turn.marshall.guice.MarshallStorageModule;
import com.turn.marshall.model.PropertyEntity;
import com.turn.marshall.model.PropertyGroupEntity;
import com.turn.marshall.model.PropertyScopeEntity;
import com.turn.marshall.shared.AppStage;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.PropertyKey;
import com.turn.marshall.shared.PropertyScope;
import com.turn.marshall.shared.ScopeType;

/**
 * @author Daniel Liu
 */
@Test(groups = { "unittest" })
public class PropertyStorageCacheTest
{
    private static PropertyStorageCache cache;
    private static PropertyStorageService service;
    private static PropertyGroupStorageService groupService;
    private static PropertyScopeStorageService scopeService;

    @BeforeClass
    public void initialize()
    {
        Injector injector = Guice.createInjector(Stage.PRODUCTION,  new MarshallStorageModule(AppStage.DEVELOPMENT, StorageRepository.BDB));
        cache = injector.getInstance(PropertyStorageCache.class);
        service = injector.getInstance(PropertyStorageService.class);
        scopeService = injector.getInstance(PropertyScopeStorageService.class);
        groupService = injector.getInstance(PropertyGroupStorageService.class);
    }

    @Test(enabled = false)  // broken
    public void basicCacheTest() throws RepositoryException
    {
    	final String groupName = "group-1";
    	final String keyName = "key-1";
    	final String keyValue = "value-1";
    	final ScopeType scopeType = ScopeType.HOST_NAME;
    	final String scopeValue = "turn.com";
    	
    	// scope
     	PropertyScopeEntity propertyScopeEntity = scopeService.prepare();
     	propertyScopeEntity.setPropertyScopeType(scopeType);
     	propertyScopeEntity.setPropertyScope(scopeValue);
     	propertyScopeEntity.setEntityStatus(EntityStatus.ACTIVED);
     	propertyScopeEntity = scopeService.insert(propertyScopeEntity, "tester", DateTime.now(), "test create");
        assertNotNull(propertyScopeEntity);

        // group
        PropertyGroupEntity propertyGroupEntity = groupService.prepare();
    	propertyGroupEntity.setPropertyGroup(groupName);
    	propertyGroupEntity.setEntityStatus(EntityStatus.ACTIVED);
		propertyGroupEntity = groupService.insert(propertyGroupEntity, "tester", DateTime.now(), "test create");
        assertNotNull(propertyGroupEntity);
    	
        PropertyEntity entity = service.prepare();
        entity.setPropertyKey(keyName);
        entity.setPropertyValue(keyValue);
        entity.setEntityStatus(EntityStatus.ACTIVED);
        entity.setPropertyScope(propertyScopeEntity);
        entity.setPropertyGroup(propertyGroupEntity);
		
        entity = service.insert(entity, "tester", DateTime.now(), "test create");
        assertNotNull(entity);
        
		PropertyScope scope = PropertyScope.create();
		scope.addScope(scopeType, scopeValue);
		assertEquals(cache.getProperty(scope, PropertyKey.create(keyName, groupName)), keyValue);
		assertEquals(cache.getProperty(scope, PropertyKey.create("DifferentKey", groupName)), null);

		scopeService.delete(propertyScopeEntity);
		groupService.delete(propertyGroupEntity);
        service.delete(entity);
    }

    @Test(enabled = false)  // broken
    public void getPropertyTest() throws RepositoryException
    {
    	final String groupName = "group";
    	final String keyName = "key";

    	final String keyValue1 = "value-1";
    	final String keyValue2 = "value-2";
    	final String keyValue3 = "value-3";
    	final ScopeType scopeType1 = ScopeType.HOST_NAME;
    	final String scopeValue1 = "turn.com";
    	final ScopeType scopeType2 = ScopeType.SERVER_ID;
    	final String scopeValue2 = "app181";
    	
    	// scope 1
     	PropertyScopeEntity propertyScopeEntity1 = scopeService.prepare();
     	propertyScopeEntity1.setPropertyScopeType(scopeType1);
     	propertyScopeEntity1.setPropertyScope(scopeValue1);
     	propertyScopeEntity1.setEntityStatus(EntityStatus.ACTIVED);
     	propertyScopeEntity1 = scopeService.insert(propertyScopeEntity1, "tester", DateTime.now(), "test create");
        assertNotNull(propertyScopeEntity1);
        
        // scope 2
     	PropertyScopeEntity propertyScopeEntity2 = scopeService.prepare();
     	propertyScopeEntity2.setPropertyScopeType(scopeType2);
     	propertyScopeEntity2.setPropertyScope(scopeValue2);
     	propertyScopeEntity2.setEntityStatus(EntityStatus.ACTIVED);
     	propertyScopeEntity2 = scopeService.insert(propertyScopeEntity2, "tester", DateTime.now(), "test create");
        assertNotNull(propertyScopeEntity2);

        // group
        PropertyGroupEntity propertyGroupEntity = groupService.prepare();
    	propertyGroupEntity.setPropertyGroup(groupName);
    	propertyGroupEntity.setEntityStatus(EntityStatus.ACTIVED);
		propertyGroupEntity = groupService.insert(propertyGroupEntity, "tester", DateTime.now(), "test create");
        assertNotNull(propertyGroupEntity);
    	
        // entity 1 (no group)
        PropertyEntity entity1 = service.prepare();
        entity1.setPropertyKey(keyName);
        entity1.setPropertyValue(keyValue1);
        entity1.setEntityStatus(EntityStatus.ACTIVED);
        entity1.setPropertyScope(propertyScopeEntity1);
        entity1 = service.insert(entity1, "tester", DateTime.now(), "test create");
        assertNotNull(entity1);
        
        // entity 2 (has both scope and group)
        PropertyEntity entity2 = service.prepare();
        entity2.setPropertyKey(keyName);
        entity2.setPropertyValue(keyValue2);
        entity2.setEntityStatus(EntityStatus.ACTIVED);
        entity2.setPropertyScope(propertyScopeEntity2);
        entity2.setPropertyGroup(propertyGroupEntity);
        entity2 = service.insert(entity2, "tester", DateTime.now(), "test create");
        assertNotNull(entity2);
        
        // entity 3 (no scope)
        PropertyEntity entity3 = service.prepare();
        entity3.setPropertyKey(keyName);
        entity3.setPropertyValue(keyValue3);
        entity3.setPropertyGroup(propertyGroupEntity);
        entity3.setEntityStatus(EntityStatus.ACTIVED);
        entity3 = service.insert(entity3, "tester", DateTime.now(), "test create");
        assertNotNull(entity3);
        
        // check 1
		PropertyScope scope1 = PropertyScope.create();
		scope1.addScope(scopeType1, scopeValue1);
		assertEquals(cache.getProperty(scope1, PropertyKey.create(keyName, groupName)), keyValue3);
		assertEquals(cache.getProperty(scope1, PropertyKey.create(keyName, null)), keyValue1);
		
		// check 2 
		PropertyScope scope = PropertyScope.create();
		scope.addScope(scopeType1, scopeValue1);
		assertEquals(cache.getProperty(scope, PropertyKey.create(keyName, groupName)), keyValue3);
		scope.addScope(scopeType2, scopeValue2);
		assertEquals(cache.getProperty(scope, PropertyKey.create(keyName, groupName)), keyValue2);
		assertEquals(cache.getProperty(scope, PropertyKey.create(keyName, null)), keyValue1);
		
		// No scope entity
		assertEquals(cache.getProperties(scope).size(), 2);

		scopeService.delete(propertyScopeEntity1);
		scopeService.delete(propertyScopeEntity2);
		groupService.delete(propertyGroupEntity);
        service.delete(entity1);
        service.delete(entity2);
        service.delete(entity3);
    }

    @Test(enabled = false)  // broken
    public void getPropertiesTest() throws RepositoryException
    {
    	final String keyName = "key";
    	final String keyName4 = "key4";
    	final String keyValue1 = "value-1";
    	final String keyValue2 = "value-2";
    	final String keyValue3 = "value-3";
    	final String keyValue4 = "value-4";
    	final ScopeType scopeType1 = ScopeType.HOST_NAME;
    	final String scopeValue1 = "turn.com";
    	final ScopeType scopeType2 = ScopeType.SERVER_ID;
    	final String scopeValue2 = "app181";
    	
    	// scope 1
     	PropertyScopeEntity propertyScopeEntity1 = scopeService.prepare();
     	propertyScopeEntity1.setPropertyScopeType(scopeType1);
     	propertyScopeEntity1.setPropertyScope(scopeValue1);
     	propertyScopeEntity1.setEntityStatus(EntityStatus.ACTIVED);
     	propertyScopeEntity1 = scopeService.insert(propertyScopeEntity1, "tester", DateTime.now(), "test create");
        assertNotNull(propertyScopeEntity1);
        
        // scope 2
     	PropertyScopeEntity propertyScopeEntity2 = scopeService.prepare();
     	propertyScopeEntity2.setPropertyScopeType(scopeType2);
     	propertyScopeEntity2.setPropertyScope(scopeValue2);
     	propertyScopeEntity2.setEntityStatus(EntityStatus.ACTIVED);
     	propertyScopeEntity2 = scopeService.insert(propertyScopeEntity2, "tester", DateTime.now(), "test create");
        assertNotNull(propertyScopeEntity2);

        // entity 1
        PropertyEntity entity1 = service.prepare();
        entity1.setPropertyKey(keyName);
        entity1.setPropertyValue(keyValue1);
        entity1.setEntityStatus(EntityStatus.ACTIVED);
        entity1.setPropertyScope(propertyScopeEntity1);
        entity1 = service.insert(entity1, "tester", DateTime.now(), "test create");
        assertNotNull(entity1);
        
        // entity 2
        PropertyEntity entity2 = service.prepare();
        entity2.setPropertyKey(keyName);
        entity2.setPropertyValue(keyValue2);
        entity2.setEntityStatus(EntityStatus.ACTIVED);
        entity2.setPropertyScope(propertyScopeEntity2);
        entity2 = service.insert(entity2, "tester", DateTime.now(), "test create");
        assertNotNull(entity2);
        
        // entity 3 (no scope)
        PropertyEntity entity3 = service.prepare();
        entity3.setPropertyKey(keyName);
        entity3.setPropertyValue(keyValue3);
        entity3.setEntityStatus(EntityStatus.ACTIVED);
        entity3 = service.insert(entity3, "tester", DateTime.now(), "test create");
        assertNotNull(entity3);
        
        // entity 4 (no scope)
        PropertyEntity entity4 = service.prepare();
        entity4.setPropertyKey(keyName4);
        entity4.setPropertyValue(keyValue4);
        entity4.setEntityStatus(EntityStatus.ACTIVED);
        entity4 = service.insert(entity4, "tester", DateTime.now(), "test create");
        assertNotNull(entity4);
		
        // check 1
        PropertyScope scopeA = PropertyScope.create();
        Map<PropertyKey, String> map = cache.getProperties(scopeA);
		assertEquals(map.size(), 2);
		assertEquals(map.get(PropertyKey.create(keyName, null)), keyValue3);
		assertEquals(map.get(PropertyKey.create(keyName4, null)), keyValue4);
		
		// check 2.0
		scopeA.addScope(scopeType1, scopeValue1);
		map = cache.getProperties(scopeA);
		assertEquals(map.size(), 2);
		assertEquals(map.get(PropertyKey.create(keyName, null)), keyValue1);
		assertEquals(map.get(PropertyKey.create(keyName4, null)), keyValue4);
		
		// check 2.1
		scopeA.addScope(scopeType2, scopeValue2);
		map = cache.getProperties(scopeA);
		assertEquals(map.size(), 2);
		assertEquals(map.get(PropertyKey.create(keyName, null)), keyValue2);
		assertEquals(map.get(PropertyKey.create(keyName4, null)), keyValue4);
		
		// check 3
		PropertyScope scopeB = PropertyScope.create();
		scopeB.addScope(scopeType2, scopeValue2);
		map = cache.getProperties(scopeB);
		assertEquals(map.size(), 2);
		assertEquals(map.get(PropertyKey.create(keyName, null)), keyValue2);
		assertEquals(map.get(PropertyKey.create(keyName4, null)), keyValue4);
		
		
		scopeService.delete(propertyScopeEntity1);
		scopeService.delete(propertyScopeEntity2);
        service.delete(entity1);
        service.delete(entity2);
        service.delete(entity3);
        service.delete(entity4);
    }
}
