/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.storage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.util.List;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

import org.joda.time.DateTime;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.turn.marshall.guice.MarshallStorageModule;
import com.turn.marshall.model.PropertyScopeEntity;
import com.turn.marshall.shared.AppStage;
import com.turn.marshall.shared.EntityStatus;
import com.turn.marshall.shared.ScopeType;

/**
 * @author Chunzhao Zheng
 */
@Test(groups = { "unittest" })
public class PropertyScopeStorageServiceTest
{
    private static PropertyScopeStorageService service;

    @BeforeClass
    public void initialize()
    {
        Injector injector = Guice.createInjector(Stage.PRODUCTION, new MarshallStorageModule(AppStage.DEVELOPMENT, StorageRepository.BDB));
        service = injector.getInstance(PropertyScopeStorageService.class);
    }

    @Test
    public void createPropertyScopeEntity() throws RepositoryException
    {
        PropertyScopeEntity entity = service.prepare();
        entity.setPropertyScopeType(ScopeType.DATA_CENTER);
        entity.setPropertyScope("ds-1");
        entity.setEntityStatus(EntityStatus.ACTIVED);

        entity = service.insert(entity, "tester", DateTime.now(), "test create");
        assertNotNull(entity);

        service.delete(entity);
    }

    @Test
    public void findPropertyEntity() throws RepositoryException
    {
        PropertyScopeEntity entity1 = service.prepare();
        entity1.setPropertyScopeType(ScopeType.DATA_CENTER);
        entity1.setPropertyScope("ds-1");
        entity1.setEntityStatus(EntityStatus.ACTIVED);
        entity1 = service.insert(entity1, "tester", DateTime.now(), "test create");

        PropertyScopeEntity entity2 = service.prepare();
        entity2.setPropertyScopeType(ScopeType.HOST_NAME);
        entity2.setPropertyScope("host-1");
        entity2.setEntityStatus(EntityStatus.ACTIVED);
        entity2 = service.insert(entity2, "tester", DateTime.now(), "test create");

        List<PropertyScopeEntity> entities = service.loadAll();
        assertNotNull(entities);
        assertEquals(entities.size(), 2);

        service.delete(entity1);
        service.delete(entity2);
    }
}
