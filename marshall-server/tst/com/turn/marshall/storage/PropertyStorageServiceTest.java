/**
 * Copyright (C) 2013 Turn Inc.  All Rights Reserved.
 * Proprietary and confidential.
 */
package com.turn.marshall.storage;

import static org.testng.Assert.*;

import java.util.List;

import com.amazon.carbonado.RepositoryException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

import org.joda.time.DateTime;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.turn.marshall.guice.MarshallStorageModule;
import com.turn.marshall.model.PropertyEntity;
import com.turn.marshall.shared.AppStage;
import com.turn.marshall.shared.EntityStatus;

/**
 * @author Chunzhao Zheng
 */
@Test(groups = { "unittest" })
public class PropertyStorageServiceTest
{
    private static PropertyStorageService service;

    @BeforeClass
    public void initialize()
    {
        Injector injector = Guice.createInjector(Stage.PRODUCTION,  new MarshallStorageModule(AppStage.DEVELOPMENT, StorageRepository.BDB));
        service = injector.getInstance(PropertyStorageService.class);
    }

    @Test
    public void createPropertyEntity() throws RepositoryException
    {
        PropertyEntity entity = service.prepare();
        entity.setPropertyKey("key-1");
        entity.setPropertyValue("value-1");
        entity.setEntityStatus(EntityStatus.ACTIVED);

        PropertyEntity stored = service.insert(entity, "tester", DateTime.now(), "test create");
        assertNotNull(stored);

        service.delete(stored);
    }

    @Test
    public void findPropertyEntity() throws RepositoryException
    {
        PropertyEntity entity1 = service.prepare();
        entity1.setPropertyKey("key-1");
        entity1.setPropertyValue("value-1");
        entity1.setEntityStatus(EntityStatus.ACTIVED);
        entity1 = service.insert(entity1, "tester", DateTime.now(), "test create");

        PropertyEntity entity2 = service.prepare();
        entity2.setPropertyKey("key-2");
        entity2.setPropertyValue("value-2");
        entity2.setEntityStatus(EntityStatus.ACTIVED);
        entity2 = service.insert(entity2, "tester", DateTime.now(), "test create");

        List<PropertyEntity> entities = service.loadAll();
        assertNotNull(entities);
        assertEquals(entities.size(), 2);

        service.delete(entity1);
        service.delete(entity2);
    }
}
