'use strict';

var cache = {};

angular.module('app.entity', [])
    .controller('MainCtrl', [
        '$scope', '$modal', '$http',
        function ($scope, $modal, $http) {
            $scope.selectedScopeName = "Dropdown";
            $scope.selectedGroupName = "Dropdown";
            $scope.selectedScopeId = -1;
            $scope.selectedGroupId = -1;

            $http({method: 'GET', url: '/marshall/api/controller/groups'}).success(function(data, status, headers, config) {
              // this callback will be called asynchronously
              // when the response is available
              $scope.groupList = data;
              cache.groupList = {};
              if (data && data.length) {
                data.forEach(function(d) {
                    cache.groupList[d.groupId] = d.grouopName;
                });
              }
            }).error(function(data, status, headers, config) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
              $scope.groupList = [
                {
                    groupId: 1,
                    groupName: 'Console servers'
                },
                {
                    groupId: 2,
                    groupName: 'Presentation servers'
                }];
            });

            $http({method: 'GET', url: '/marshall/api/controller/properties'}).success(function(data, status, headers, config) {
              // this callback will be called asynchronously
              // when the response is available
              $scope.properties = data;
            }).error(function(data, status, headers, config) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
              $scope.properties = [
                {
                    key: 'Key1',
                    value: 'Value1',
                    groupId: 'groupId1',
                    entityId: 'entityId1',
                    entityStatus: 'entityStatus1'
                },
                {
                    key: 'Key2',
                    value: 'Value2',
                    groupId: 'groupId2',
                    entityId: 'entityId2',
                    entityStatus: 'entityStatus2'
                },
                {
                    key: 'Key3',
                    value: 'Value3',
                    groupId: 'groupId3',
                    entityId: 'entityId3',
                    entityStatus: 'entityStatus3'
                }
            ];
            });


            $http({method: 'GET', url: '/marshall/api/controller/scopes'}).success(function(data, status, headers, config) {
              // this callback will be called asynchronously
              // when the response is available
              $scope.scopeList = data;
               cache.scopeList = {};
              if (data && data.length) {
                data.forEach(function(d) {
                    cache.groupList[d.scopeId] = d.scopeName;
                });
              }
            }).error(function(data, status, headers, config) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
            $scope.scopeList = [
                {
                    scopeId: 1,
                    scopeName: 'SJC2'
                },
                {
                    scopeId: 2,
                    scopeName: 'AMS1'
                },
                {
                    scopeId: 3,
                    scopeName: '172.69.75.3'
                }
               ];
            });

                $scope.changeScopeValue = function(index) {
                    $scope.selectedScopeName = $scope.scopeList[index].scopeName;
                    $scope.selectedScopeId = $scope.scopeList[index].scopeId;

                    $http({method: 'GET', url: '/marshall/api/controller/properties?groupId='+$scope.selectedGroupId+'&scopeId='+$scope.selectedScopeId}).success(function(data, status, headers, config) {
                      // this callback will be called asynchronously
                      // when the response is available
                    $scope.properties = data;
                }).error(function(data, status, headers, config) {
                      // called asynchronously if an error occurs
                          // or server returns response with an error status.
                    $scope.properties = [
                    {
                        key: 'Key1',
                        value: 'Value1',
                        groupId: 'groupId1',
                        entityId: 'entityId1',
                        entityStatus: 'entityStatus1'
                    },
                    {
                        key: 'Key2',
                        value: 'Value2',
                        groupId: 'groupId2',
                        entityId: 'entityId2',
                        entityStatus: 'entityStatus2'
                    },
                    {
                        key: 'Key3',
                        value: 'Value3',
                        groupId: 'groupId3',
                        entityId: 'entityId3',
                        entityStatus: 'entityStatus3'
                    }];
                });
                };

                $scope.changeGroupValue = function(index) {
                    $scope.selectedGroupName = $scope.groupList[index].groupName;
                    $scope.selectedGroupId = $scope.groupList[index].groupId;

                    $http({method: 'GET', url: '/marshall/api/controller/properties?groupId='+$scope.selectedGroupId+'&scopeId='+$scope.selectedScopeId}).success(function(data, status, headers, config) {
                      // this callback will be called asynchronously
                      // when the response is available
                    $scope.properties = data;
                }).error(function(data, status, headers, config) {
                      // called asynchronously if an error occurs
                          // or server returns response with an error status.
                    $scope.properties = [
                    {
                        key: 'Key1',
                        value: 'Value1',
                        groupId: 'groupId1',
                        entityId: 'entityId1',
                        entityStatus: 'entityStatus1'
                    },
                    {
                        key: 'Key2',
                        value: 'Value2',
                        groupId: 'groupId2',
                        entityId: 'entityId2',
                        entityStatus: 'entityStatus2'
                    },
                    {
                        key: 'Key3',
                        value: 'Value3',
                        groupId: 'groupId3',
                        entityId: 'entityId3',
                        entityStatus: 'entityStatus3'
                    }];
                });
            };

            $scope.add = function(property) {
                var modalInstance = $modal.open({
                    templateUrl: 'addProperty.html',
                    controller: 'addPropertyCtrl',
                    resolve: {
                        item: function () {
                            return {
                                selectedScopeId: $scope.selectedScopeId,
                                selectedGroupId: $scope.selectedGroupId
                            };
                        }
                    }
                });

                modalInstance.result.then(function (property){
                    property.reason = "I want to add?";
                    $http({method: 'POST', data: property, url: '/marshall/api/controller/property'}).success(function(data, status, headers, config) {
                          // this callback will be called asynchronously
                          // when the response is available
                      $scope.properties.unshift(property);
                    }).error(function(data, status, headers, config) {

                    });
                });
            };

            $scope.edit = function(property) {
                var modalInstance = $modal.open({
                    templateUrl: 'editProperty.html',
                    controller: 'editPropertyCtrl',
                    resolve: {
                        property: function () {
                            return property;
                        },
                        dropdownList: function() {
                            var dropdownList = {};
                            dropdownList.scopeList = $scope.scopeList;
                            dropdownList.groupList = $scope.groupList;
                            return dropdownList;
                        }
                    }
                });

                modalInstance.result.then(function (newProperty){
                    var index = $scope.properties.indexOf(property);
                    newProperty.reason = "I want to update!";
                    $http({method: 'PUT', data: newProperty, url: '/marshall/api/controller/property'}).success(function(data, status, headers, config) {
                      // this callback will be called asynchronously
                      // when the response is available
                      $scope.properties.splice(index, 1, newProperty);
                    }).error(function(data, status, headers, config) {

                    });
                });
            };

            $scope.remove = function(property) {
                var modalInstance = $modal.open({
                    templateUrl: 'removeProperty.html',
                    controller: 'removePropertyCtrl',
                    resolve: {
                        property: function () {
                            return {
                                selectedScopeId: $scope.selectedScopeId,
                                selectedGroupId: $scope.selectedGroupId
                            };
                        }
                    }
                });

                modalInstance.result.then(function (remove){
                    if(remove) {
                        var index = $scope.properties.indexOf(property);
                        var deleteData = {
                            entityId: $scope.properties[index].entityId,
                            reason: "I want to delete!"
                        };

                        $http({method: 'DELETE', data: deleteData, url: '/marshall/api/controller/property'}).success(function(data, status, headers, config) {
                          // this callback will be called asynchronously
                          // when the response is available
                          $scope.properties.splice(index, 1);
                        }).error(function(data, status, headers, config) {

                        });
                    }
                });
            };
        }
    ])
    .controller('addPropertyCtrl', [
        '$scope', '$modalInstance', 'item',
        function ($scope, $modalInstance, item) {
            $scope.property = {
                key: '',
                value: '',
                groupId: item.selectedGroupId,
                scopeId: item.selectedScopeId
            }

            $scope.add = function(property) {
                $modalInstance.close($scope.property);
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        }
    ])
    .controller('editPropertyCtrl', [
        '$scope', '$modalInstance', 'property', 'dropdownList', 
        function ($scope, $modalInstance, property, dropdownList) {
            $scope.newProperty = angular.copy(property);

            $scope.save = function() {
                $modalInstance.close($scope.newProperty);
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.groupList = dropdownList.groupList;
            $scope.scopeList = dropdownList.scopeList;
        }
    ])   
    .controller('removePropertyCtrl', [
        '$scope', '$modalInstance', 'property',
        function ($scope, $modalInstance, property) {
            $scope.yes = function() {
                $modalInstance.close(true);
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        }
    ]);


angular.module('app.audit', [])
    .controller('AuditCtrl', [
        '$scope', '$modal', '$routeParams', '$http', 
        function ($scope, $modal, $routeParams, $http) {

            $scope.selectedGroupId = $routeParams.groupId;
            $scope.selectedScopeId = $routeParams.scopeId;
            $scope.selectedGroupName = $routeParams.groupName;
            $scope.selectedScopeName = $routeParams.scopeName;

            $http({method: 'GET', url: '/marshall/api/controller/audit?entityId='+$routeParams.entityId}).success(function(data, status, headers, config) {
              // this callback will be called asynchronously
              // when the response is available
              $scope.auditList = data;
              if (data && data.length) {
                $scope.auditKey = data[0].key;
                $scope.auditGroupId = data[0].groupId;
                $scope.auditScopeId = data[0].scopeId;
                $scope.auditGroupName = cache[data[0].groupId];
                $scope.auditScopeNane = cache[data[0].scopeId];
              }
            }).error(function(data, status, headers, config) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
              $scope.groupList = [
                {
                    groupId: 1,
                    groupName: 'Console servers'
                },
                {
                    groupId: 2,
                    groupName: 'Presentation servers'
                }
               ];
            });

            $scope.revert = function() {
                var modalInstance = $modal.open({
                    templateUrl: 'revertProperty.html',
                    controller: 'revertPropertyCtrl'
                });
            };
        }
    ])
    .controller('revertPropertyCtrl', [
        '$scope', '$modalInstance',
        function ($scope, $modalInstance) {

            $scope.revert = function() {
                $modalInstance.close();
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        }
    ]);